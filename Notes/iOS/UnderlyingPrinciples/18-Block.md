[TOC]

## block的本质

- block本质上也是一个OC对象，它内部也有个isa指针

- block是封装了函数调用以及函数调用环境的OC对象

- block的底层结构如下图所示


![01](./assets/imgs/18-Block/01.png)



### block的变量捕获（capture）

> 为了保证block内部能够正常访问外部的变量，block有个变量捕获机制

![02](./assets/imgs/18-Block/02.png)



### auto变量的捕获

![03](./assets/imgs/18-Block/03.png)

### block的类型

> block有3种类型，可以通过调用class方法或者isa指针查看具体类型，最终都是继承自NSBlock类型

```objective-c
__NSGlobalBlock （ _NSConcreteGlobalBlock ）
__NSStackBlock （ _NSConcreteStackBlock ）
__NSMallocBlock （ _NSConcreteMallocBlock ）
```

![04](./assets/imgs/18-Block/04.png)

![05](./assets/imgs/18-Block/05.png)



每一种类型的block调用copy后的结果如下所示

![06](./assets/imgs/18-Block/06.png)



### block的copy

- 在ARC环境下，编译器会根据情况自动将栈上的block复制到堆上，比如以下情况

  - block作为函数返回值时
  - 将block赋值给__strong指针时
  - block作为Cocoa API中方法名含有usingBlock的方法参数时
  - block作为GCD API的方法参数时

- MRC下block属性的建议写法

```objective-c
@property (copy, nonatomic) void (^block)(void);
```



- ARC下block属性的建议写法

```objective-c
@property (strong, nonatomic) void (^block)(void);
@property (copy, nonatomic) void (^block)(void); // 推荐：兼容MRC
```



### 对象类型的auto变量

- 当block内部访问了对象类型的auto变量时

  - 如果block是在栈上，将不会对auto变量产生强引用



  - 如果block被拷贝到堆上

    - 会调用block内部的copy函数
    - copy函数内部会调用_Block_object_assign函数
    - _Block_object_assign函数会根据auto变量的修饰符（`__strong、__weak、__unsafe_unretained`）做相应的操作，形成强引用（retain）或者弱引用

  - 如果block从堆上移除

    - 会调用block内部的dispose函数
    - dispose函数内部会调用_Block_object_dispose函数
    - _Block_object_dispose函数会自动释放引用的auto变量（release）

![07](./assets/imgs/18-Block/07.png)



### __weak问题解决

在使用clang转换OC为C++代码时，可能会遇到以下问题
​	cannot create __weak reference in file using manual reference

解决方案：支持ARC、指定运行时系统版本，比如

```shell
xcrun -sdk iphoneos clang -arch arm64 -rewrite-objc -fobjc-arc -fobjc-runtime=ios-8.0.0 main.m
```



### __block修饰符

> __block可以用于解决block内部无法修改auto变量值的问题
>
> __block不能修饰全局变量、静态变量（static）
>
> 编译器会将__block变量包装成一个对象

![08](./assets/imgs/18-Block/08.png)

#### __block的内存管理

- 当block在栈上时，并不会对__block变量产生强引用



- 当block被copy到堆时
  - 会调用block内部的copy函数
  - copy函数内部会调用_Block_object_assign函数
  - _Block_object_assign函数会对__block变量形成强引用（retain）

![09](./assets/imgs/18-Block/09.png)



- 当block从堆中移除时
  - 会调用block内部的dispose函数
  - dispose函数内部会调用_Block_object_dispose函数
  - _Block_object_dispose函数会自动释放引用的__block变量（release）

![10](./assets/imgs/18-Block/10.png)



#### __block的__forwarding指针

![11](./assets/imgs/18-Block/11.png)



#### 对象类型的auto变量、__block变量

- 当block在栈上时，对它们都不会产生强引用



- 当block拷贝到堆上时，都会通过copy函数来处理它们

  - __block变量（假设变量名叫做a）

  ```c++
  _Block_object_assign((void)&dst->a, (void)src->a, 8/BLOCK_FIELD_IS_BYREF/);
  ```
  
    - 对象类型的auto变量（假设变量名叫做p）
  ```c++
  _Block_object_assign((void)&dst->p, (void)src->p, 3/BLOCK_FIELD_IS_OBJECT/);
  ```




  - 当block从堆上移除时，都会通过dispose函数来释放它们

    - __block变量（假设变量名叫做a）

    ```c++
    _Block_object_dispose((void)src->a, 8/BLOCK_FIELD_IS_BYREF*/);
    ```

      - 对象类型的auto变量（假设变量名叫做p）

    ```c++
    _Block_object_dispose((void)src->p, 3/BLOCK_FIELD_IS_OBJECT*/);
    ```

  ![12](./assets/imgs/18-Block/12.png)



#### 被__block修饰的对象类型

- 当__block变量在栈上时，不会对指向的对象产生强引用



- 当__block变量被copy到堆时

  - 会调用__block变量内部的copy函数
  - copy函数内部会调用_Block_object_assign函数
  - _Block_object_assign函数会根据所指向对象的修饰符（`__strong、__weak、__unsafe_unretained`）做出相应的操作，形成强引用（retain）或者弱引用（注意：这里仅限于ARC时会retain，MRC时不会retain）

- 如果__block变量从堆上移除

  - 会调用__block变量内部的dispose函数_
  - dispose函数内部会调用_Block_object_dispose函数
  - _Block_object_dispose函数会自动释放指向的对象（release）



### 循环引用问题

![13](./assets/imgs/18-Block/13.png)



#### 解决循环引用问题 - ARC

- 用`__weak`、`__unsafe_unretained`解决

![14](./assets/imgs/18-Block/14.png)



- 用`__block`解决（必须要调用block）

![15](./assets/imgs/18-Block/15.png)



#### 解决循环引用问题 - MRC

- 用`__unsafe_unretained`解决

![16](./assets/imgs/18-Block/16.png)



- 用`__block`解决

![17](./assets/imgs/18-Block/17.png)






### 面试题

#### block的原理是怎样的？本质是什么？

封装了函数调用以及调用环境的OC对象




#### __block的作用是什么？有什么使用注意点？

- 作用

  - 编译器会将__block变量包装成一个对象
  - 可以用于解决block内部无法修改auto变量值的问题

- 使用注意点

  - 内存管理的问题
  - 循环引用问题
  - ARC环境需要在block里面将oc对象赋值nil，并且需要必须调用block
  - MRC环境不会对OC对象形成强引用



#### block的属性修饰词为什么是copy？使用block有哪些使用注意？

- block一旦没有进行copy操作，就不会在堆上
- 使用注意：循环引用问题



#### block在修改NSMutableArray，需不需要添加__block？

不需要，不是直接修改NSMutableArray实例对象，只是拿来使用