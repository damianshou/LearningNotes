[TOC]

## 什么是RunLoop

顾名思义

- 运行循环
- 在程序运行过程中循环做一些事情

![01](./assets/imgs/20-RunLoop/01.png)



应用范畴

- 定时器（Timer）、PerformSelector
- GCD Async Main Queue
- 事件响应、手势识别、界面刷新
- 网络请求
- AutoreleasePool



### 如果<font color=red>没有</font>RunLoop

> 执行完业务代码，会即将退出程序

```objective-c
int main(int argc, const char * argv[]) {
    @autoreleasepool {
        NSLog(@"Hello, World!"); // 执行完这句代码，会即将退出程序
    }
    return 0;
}
```



### 如果<font color=red>有</font>RunLoop

> 程序并不会马上退出，而是保持运行状态

```objective-c
int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc,
                                 argv,
                                 nil,
                                 NSStringFromClass([AppDelegate class]));
    }
    
}
```

伪代码

```objective-c
int main(int argc, const char * argv[]) {
    @autoreleasepool {
        int retVal = 0;
        do {
            // 睡眠中等待消息
            int msg = sleep_and_wait();
            // 处理消息
            retVal = process_message(msg)
        } while (0 == retVal);
         return 0;
    }
}
```



RunLoop的基本作用

- 保持程序的持续运行
- 处理App中的各种事件（比如触摸事件、定时器事件等）
- 节省CPU资源，提高程序性能：该做事时做事，该休息时休息
- ......



### RunLoop对象

OS中有2套API来访问和使用RunLoop

- Foundation：NSRunLoop
- Core Foundation：CFRunLoopRef



NSRunLoop和CFRunLoopRef都代表着RunLoop对象

- NSRunLoop是基于CFRunLoopRef的一层OC包装
- CFRunLoopRef是开源的
  - https://opensource.apple.com/tarballs/CF/



![02](./assets/imgs/20-RunLoop/02.png)



### RunLoop与线程

1. 每条线程都有唯一的一个与之对应的RunLoop对象
2. RunLoop保存在一个全局的Dictionary里，线程作为key，RunLoop作为value
3. 线程刚创建时并没有RunLoop对象，RunLoop会在第一次获取它时创建
4. RunLoop会在线程结束时销毁
5. 主线程的RunLoop已经自动获取（创建），子线程默认没有开启RunLoop



### 获取RunLoop对象

Foundation

```objective-c
[NSRunLoop currentRunLoop];	// 获得当前线程的RunLoop对象
[NSRunLoop mainRunLoop]; 	// 获得主线程的RunLoop对象
```

Core Foundation

```c
CFRunLoopGetCurrent();	// 获得当前线程的RunLoop对象
CFRunLoopGetMain(); 	// 获得主线程的RunLoop对象
```



### RunLoop相关的类

Core Foundation中关于RunLoop的5个类

- CFRunLoopRef
- CFRunLoopModeRef
- CFRunLoopSourceRef
- CFRunLoopTimerRef
- CFRunLoopObserverRef

```objective-c
typedef struct __CFRunLoop * CFRunLoopRef;
struct __CFRunLoop {
    pthread_t _pthread;
    CFMutableSetRef _commonModes;
    CFMutabluSetRef _commonModeItems;
    CFRunLoopModeRef _currentMode;
    CFMutableSetRef _modes;
};

typedef struct __CFRunLoopMode *CFRunLoopModeRef;
struct __CFRunLoop {
    CFStringRef _name;
    CFMutableSetRef _sources0;
    CFMutableSetRef _sources1;
    CFMutableArrayRef _observers;
    CFMutableArrayRef _times;
};
```

![03](./assets/imgs/20-RunLoop/03.png)



#### CFRunLoopModeRef

1. CFRunLoopModeRef代表RunLoop的运行模式
2. 一个RunLoop包含若干个Mode，每个Mode又包含若干个Source0/Source1/Timer/Observer
3. RunLoop启动时只能选择其中一个Mode，作为currentMode
4. 如果需要切换Mode，只能退出当前Loop，再重新选择一个Mode进入
   - 不同组的Source0/Source1/Timer/Observer能分隔开来，互不影响
5. 如果Mode里没有任何Source0/Source1/Timer/Observer，RunLoop会立马退出



#### CFRunLoopModeRef

常见的2种Mode

1. **kCFRunLoopDefaultMode**（NSDefaultRunLoopMode）：App的默认Mode，通常主线程是在这个Mode下运行
2. **UITrackingRunLoopMode**：界面跟踪 Mode，用于 ScrollView 追踪触摸滑动，保证界面滑动时不受其他 Mode 影响



#### CFRunLoopObserverRef

```objective-c
/* Run Loop Observer Activities */
typedef CF_OPTIONS(CFOptionFlags, CFRunLoopActivity) {
    kCFRunLoopEntry = (1UL << 0),			// 即将进入Loop
    kCFRunLoopBeforeTimers = (1UL << 1),	// 即将处理Timer
    kCFRunLoopBeforeSources = (1UL << 2),	// 即将处理Source
    kCFRunLoopBeforeWaiting = (1UL << 5),	// 即将进入休眠
    kCFRunLoopAfterWaiting = (1UL << 6),	// 刚从休眠中唤醒
    kCFRunLoopExit = (1UL << 7),			// 即将退出Loop
    kCFRunLoopAllActivities = 0x0FFFFFFFU
};
```



##### 添加Observer监听RunLoop的所有状态

```objective-c
// 创建Observer
CFRunLoopObserverRef observer = nil
observer = CFRunLoopObserverCreateWithHandler(kCFAllocatorDefault,
                                              kCFRunLoopAllActivities,
                                              YES,
                                              0,
                                              ^(CFRunLoopObserverRef observer,
                                              CFRunLoopActivity activity) {
        switch (activity) {
        case kCFRunLoopEntry:
            NSLog(@"kCFRunLoopEntry");
            break;
        case kCFRunLoopBeforeTimers:
            NSLog(@"kCFRunLoopBeforeTimers");
            break;
        case kCFRunLoopBeforeSources:
            NSLog(@"kCFRunLoopBeforeSources");
            break;
        case kCFRunLoopBeforeWaiting:
            NSLog(@"kCFRunLoopBeforeWaiting");
            break;
        case kCFRunLoopAfterWaiting:
            NSLog(@"kCFRunLoopAfterWaiting");
            break;
        case kCFRunLoopExit:
            NSLog(@"kCFRunLoopExit");
            break;
        default:
            break;
    }
});
// 添加Observer到RunLoop中
CFRunLoopAddObserver(CFRunLoopGetMain(), observer, kCFRunLoopCommonModes);
// 释放
CFRelease(observer);
```



### RunLoop的运行逻辑

![04](./assets/imgs/20-RunLoop/04.png)



#### Source0

- 触摸事件处理
- performSelector:onThread:




#### Source1

- 基于Port的线程间通信
- 系统事件捕捉



#### Timers

- NSTimer
- performSelector:withObject:afterDelay:



#### Observers

- 用于监听RunLoop的状态
- UI刷新（BeforeWaiting）
- Autorelease pool（BeforeWaiting）



#### 运行逻辑

01、通知Observers：进入Loop
02、通知Observers：即将处理Timers
03、通知Observers：即将处理Sources
04、处理Blocks
05、处理Source0（可能会再次处理Blocks）
06、如果存在Source1，就跳转到第8步
07、通知Observers：开始休眠（等待消息唤醒）
08、通知Observers：结束休眠（被某个消息唤醒）
​	01> 处理Timer
​	02> 处理GCD Async To Main Queue
​	03> 处理Source1
09、处理Blocks
10、根据前面的执行结果，决定如何操作
​	01> 回到第02步
​	02> 退出Loop
11、通知Observers：退出Loop

![05](./assets/imgs/20-RunLoop/05.png)



### RunLoop休眠的实现原理

![06](./assets/imgs/20-RunLoop/06.png)



### RunLoop在实际开中的应用

1. 控制线程生命周期（线程保活）
2. 解决NSTimer在滑动时停止工作的问题
3. 监控应用卡顿
4. 性能优化



### NSTime失效

> 当用户在拖动屏幕中的滚动控件（UIScrollView）时，定时器会失效

```objective-c
- (void)viewDidLoad {
    [super viewDidLoad];
    
    static int count = 0;
    // scheduledTimer 带有这个开始的方法，会直接将定时器添加到默认模式下
    [NSTimer scheduledTimerWithTimeInterval:1.0
     								repeats:YES 
     								  block:^(NSTimer * _Nonnull timer) {
        // 获取当前模式
        CFRunLoopCopyCurrentMode(CFRunLoopGetCurrent()); // NSDefaultRunLoopMode
        NSLog(@"%d", ++count);
    }];
}
```



### 解决NSTimer在滑动时停止工作的问题

```objective-c
- (void)viewDidLoad {
    [super viewDidLoad];
    
    static int count = 0;
    NSTimer *timer = [NSTimer timerWithTimeInterval:1.0 
                      						repeats:YES 
                           				      block:^(NSTimer * _Nonnull timer) {
        NSLog(@"%d", ++count);
    }];
    // 添加到 NSDefaultRunLoopMode 模式
//    [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSDefaultRunLoopMode];
    // 添加到 UITrackingRunLoopMode 模式
//    [[NSRunLoop currentRunLoop] addTimer:timer forMode:UITrackingRunLoopMode];
    
    /**
     NSRunLoopCommonModes并不是一个真的模式，它只是一个标记
     真正存在的模式：
     	NSDefaultRunLoopMode 默认模式
     	UITrackingRunLoopMode 跟踪模式
     timer能在_commonModes数组中存放的模式下工作
     */
    [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];    
}
```



### 线程保活

1. 执行完任务，线程死掉

```objective-c
#import <Foundation/Foundation.h>

@interface SLThread : NSThread
@end
    
@implementation SLThread
- (void)dealloc {
    NSLog(@"%s", __func__);
}
@end
    
#import "ViewController.h"
#import "SLThread.h"

@implementation ViewController
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    SLThread *thread = [[SLThread alloc] initWithTarget:self
                        					   selector:@selector(run) 
                           						 object:nil];
    [thread start];
}

// 子线程需要执行的任务
- (void)run {
    NSLog(@"%s %@", __func__, [NSThread currentThread]); 
} // 执行完run，线程死掉
@end
```

2. 执行完任务，线程还在

```objective-c
#import "ViewController.h"
#import "SLThread.h"

@interface ViewController ()
@property (strong, nonatomic) SLThread *thread;
@end

@implementation ViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    // 创建线程
    self.thread = [[SLThread alloc] initWithTarget:self
                   						  selector:@selector(run) 
                   							object:nil];
    // 启动线程
    [self.thread start];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self performSelector:@selector(test)
     			 onThread:self.thread 
     		   withObject:nil 
     		waitUntilDone:NO];
}

// 子线程需要执行的任务
- (void)test {
    NSLog(@"%s %@", __func__, [NSThread currentThread]);
}

// 这个方法的目的：线程保活
- (void)run {
    NSLog(@"%s %@", __func__, [NSThread currentThread]); 
    
    /** 
     保证RunLoop不会立马退出：往RunLoop里面添加Source\Timer\Observer 任意一个即可     
     forMode: 在子线不需要考虑 UITrackingRunLoopMode 刷新UI
     */
    [[NSRunLoop currentRunLoop] addPort:[[NSPort alloc] init]
                                forMode:NSDefaultRunLoopMode];  // 关键代码
    // 如果Mode里没有任何Source0/Source1/Timer/Observer，RunLoop会立马退出
    [[NSRunLoop currentRunLoop] run]; // 执行完这行代码，线程开始休眠
    
    NSLog(@"%s ----end----", __func__); // 没有打印这句，表示线程没有死
} 
@end
```



#### 存在的问题

1. 控制器 和 线程 相互持有对方（强引用）
2. 控制器 和 线程 都无法销毁



#### 解决方案

##### 方案1. 使控制器销毁

> 使用Block方式创建线程，解决相互强引用

```objective-c
#import "ViewController.h"
#import "SLThread.h"

@interface ViewController ()
@property (strong, nonatomic) SLThread *thread;
@end

@implementation ViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    // 创建线程
   self.thread = [[SLThread alloc] initWithBlock:^{
        NSLog(@"%s %@", __func__, [NSThread currentThread]); 
    
    /** 
     保证RunLoop不会立马退出：往RunLoop里面添加Source\Timer\Observer 任意一个即可     
     forMode: 在子线不需要考虑 UITrackingRunLoopMode 刷新UI
     */
    [[NSRunLoop currentRunLoop] addPort:[[NSPort alloc] init]
                                forMode:NSDefaultRunLoopMode];  // 关键代码
    // 如果Mode里没有任何Source0/Source1/Timer/Observer，RunLoop会立马退出
    [[NSRunLoop currentRunLoop] run]; // 执行完这行代码，线程开始休眠
    
    NSLog(@"%s ----end----", __func__); // 没有打印这句，表示线程没有死        
    }];
    // 启动线程
    [self.thread start];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self performSelector:@selector(test) 
     			 onThread:self.thread 
     		   withObject:nil 
     		waitUntilDone:NO];
}

- (void)delloc {
    NSLog(@"%s", __func__);
}

// 子线程需要执行的任务
- (void)test {
    NSLog(@"%s %@", __func__, [NSThread currentThread]);
}

@end
```



###### 存在的问题

控制器可以销毁，但是线程还在

> [NSRunLoop currentRunLoop] run];  
>
> 查看 run 方法调用 文档说明
>
> it runs the receiver in the NSDefaultRunLoopMode by repeatedly invoking runMode:beforeDate:.
> In other words, this method effectively begins an infinite loop that processes data from the run loop’s input sources and timers
>
> NSRunLoop的run方法是无法停止的，它专门用于开启一个永不销毁的线程（NSRunLoop）



##### 方案2. 使线程销毁

> 自己写一个 启动方法 实现

```objective-c
#import "ViewController.h"
#import "SLThread.h"

@interface ViewController ()
@property (strong, nonatomic) SLThread *thread;
@property (assign, nonatomic, getter=isStoped) BOOL stopped;
@end
    
@implementation ViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    
    __weak typeof(self) weakSelf = self;
    
    self.stopped = NO;
    self.thread = [[SLThread alloc] initWithBlock:^{
        NSLog(@"%@----begin----", [NSThread currentThread]);
        
       // 往RunLoop里面添加Source\Timer\Observer
        [[NSRunLoop currentRunLoop] addPort:[[NSPort alloc] init]
                                    forMode:NSDefaultRunLoopMode];
        // 重要：弱指针是否被清空(被销毁)
        while (weakSelf && !weakSelf.isStoped) {
            [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode
                                     beforeDate:[NSDate distantFuture]];
        }
        
        NSLog(@"%@----end----", [NSThread currentThread]);
        
    }];
    [self.thread start];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    if (!self.thread) return;
    [self performSelector:@selector(test)
                 onThread:self.thread
               withObject:nil
            waitUntilDone:NO];
}

- (void)dealloc {
    NSLog(@"%s", __func__);
    
    [self stop];
}

// 点击停止线程按钮
- (IBAction)stop {
    // 在子线程调用stop
    [self performSelector:@selector(stopThread)
                 onThread:self.thread
               withObject:nil
            waitUntilDone:YES]; // 设置为YES，代表子线程的代码执行完毕后，这个方法才会往下走
}

// 用于停止子线程的RunLoop
- (void)stopThread {
    // 设置标记为NO
    self.stopped = YES;
    
    // 停止RunLoop
    CFRunLoopStop(CFRunLoopGetCurrent());
    NSLog(@"%s %@", __func__, [NSThread currentThread]);
    
    // 如果是手动点击按钮停止线程，需清空线程
    self.thread = nil;
}

// 子线程需要执行的任务
- (void)test {
    NSLog(@"%s %@", __func__, [NSThread currentThread]);
}

@end
```



### 线程保活的封装

#### 1. 接口设计

- 继承 NSObject，不继承 NSThread，**屏蔽**系统的实现，由自己来定义
- 开启线程
- 在当前线程执行任务
  - 只负责执行，业务逻辑由外界去处理
  - 使用 Block 
- 结束线程

##### SLPermenantThread.h（接口设计）

```objective-c
//  SLPermenantThread.h

#import <Foundation/Foundation.h>

typedef void (^SLPermenantThreadTask)(void);

@interface SLPermenantThread : NSObject

/**
 在当前子线程执行一个任务
 */
- (void)executeTask:(SLPermenantThreadTask)task;

/**
 结束线程
 */
- (void)stop;

@end

```



#### 2.接口实现

##### SLPermenantThread.m（OC语言方式实现）

```objective-c
//  SLPermenantThread.m
#import "SLPermenantThread.h"

// SLThread 类型
@interface SLThread : NSThread
@end
@implementation SLThread
- (void)dealloc {
    NSLog(@"%s", __func__);
}
@end

//  SLPermenantThread.m
@interface SLPermenantThread()
@property (strong, nonatomic) SLThread *innerThread;
@property (assign, nonatomic, getter=isStopped) BOOL stopped;
@end

@implementation SLPermenantThread
    
#pragma mark - public methods
- (instancetype)init {
    if (self = [super init]) {
        self.stopped = NO;
        
        __weak typeof(self) weakSelf = self;
        
        self.innerThread = [[SLThread alloc] initWithBlock:^{
            [[NSRunLoop currentRunLoop] addPort:[[NSPort alloc] init]
                                        forMode:NSDefaultRunLoopMode];
            
            while (weakSelf && !weakSelf.isStopped) {
                [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode
                                         beforeDate:[NSDate distantFuture]];
            }
        }];
        
        [self.innerThread start];
    }
    return self;
}


- (void)executeTask:(SLPermenantThreadTask)task {
    if (!self.innerThread || !task) return;
    
    [self performSelector:@selector(__executeTask:)
                 onThread:self.innerThread
               withObject:task
            waitUntilDone:NO];
}

- (void)stop {
    if (!self.innerThread) return;
    
    [self performSelector:@selector(__stop)
                 onThread:self.innerThread
               withObject:nil
            waitUntilDone:YES];
}

- (void)dealloc {
    NSLog(@"%s", __func__);
    
    [self stop];
}

#pragma mark - private methods
- (void)__stop {
    self.stopped = YES;
    CFRunLoopStop(CFRunLoopGetCurrent());
    self.innerThread = nil;
}

- (void)__executeTask:(SLPermenantThreadTask)task {
    task();
}
@end
```

##### SLPermenantThread.m（C语言方式实现）

```objective-c
// SLPermenantThread.m

#import "SLPermenantThread.h"

/** SLThread **/
@interface SLThread : NSThread
@end
@implementation SLThread
- (void)dealloc {
    NSLog(@"%s", __func__);
}
@end

/** SLPermenantThread **/
@interface SLPermenantThread()
@property (strong, nonatomic) SLThread *innerThread;
@end

@implementation SLPermenantThread
#pragma mark - public methods
- (instancetype)init {
    if (self = [super init]) {
        self.innerThread = [[SLThread alloc] initWithBlock:^{
            
            // 创建上下文（要初始化一下结构体）
            CFRunLoopSourceContext context = {0};
            // 创建source
            CFRunLoopSourceRef source = CFRunLoopSourceCreate(kCFAllocatorDefault,
                                                              0,
                                                              &context);
            // 往Runloop中添加source
            CFRunLoopAddSource(CFRunLoopGetCurrent(),
                               source,
                               kCFRunLoopDefaultMode);
            // 销毁source
            CFRelease(source);
            
            // 启动
            CFRunLoopRunInMode(kCFRunLoopDefaultMode,
                               1.0e10,
                               false);
        }];
        
        [self.innerThread start];
    }
    return self;
}

- (void)executeTask:(SLPermenantThreadTask)task {
    if (!self.innerThread || !task) return;
    
    [self performSelector:@selector(__executeTask:)
                 onThread:self.innerThread
               withObject:task
            waitUntilDone:NO];
}

- (void)stop {
    if (!self.innerThread) return;
    
    [self performSelector:@selector(__stop)
                 onThread:self.innerThread
               withObject:nil
            waitUntilDone:YES];
}

- (void)dealloc {
    NSLog(@"%s", __func__);
    
    [self stop];
}

#pragma mark - private methods
- (void)__stop {
    CFRunLoopStop(CFRunLoopGetCurrent());
    self.innerThread = nil;
}

- (void)__executeTask:(SLPermenantThreadTask)task {
    task();
}

@end

```



#### 3.接口调用

##### ViewController.m

```objective-c
#import "ViewController.h"
#import "SLPermenantThread.h"

@interface ViewController ()
@property (strong, nonatomic) SLPermenantThread *thread;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.thread = [[SLPermenantThread alloc] init];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.thread executeTask:^{
        NSLog(@"执行任务 - %@", [NSThread currentThread]);
    }];
}

- (IBAction)stop {
    [self.thread stop];
}

- (void)dealloc {
    NSLog(@"%s", __func__);
}

@end
```



### 面试题

1. 讲讲 RunLoop，项目中有用到吗？

2. runloop内部实现逻辑？

3. runloop和线程的关系？

4. timer 与 runloop 的关系？

   - 数据结构层面
     -  runloop 里有放着一堆模式 CFMutableSetRef _modes
     -  CFMutableSetRef _modes 里面有 CFMutableArrayRef _timers
     -  如果 timer 是设置在 NSRunLoopCommonModes，timer将会放在 _commonModeItems
   - 什么时候处理Timer
     - 唤醒某个线程
     - 通知Observers：结束休眠
     - 处理Timer



5. 程序中添加每3秒响应一次的NSTimer，当拖动tableview时timer可能无法响应要怎么解决？
   将定时器添加到NSRunLoopCommonModes模式




6. runloop 是怎么响应用户操作的， 具体流程是什么样的？
   - 由Source1把系统事件捕捉（点击屏幕，由Source1实现）
   - Source1将事件包装成事件队列（EventQueue）再由Source0处理