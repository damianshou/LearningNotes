[TOC]

## Theos

### ⼀、安装签名工具ldid 

- 先确保安装了brew <https://brew.sh/>
	
	```shell
	$ /usr/bin/ruby -e "$(curl -fsSL
	https://raw.githubusercontent.com/Homebrew/install/master/install)"
	```
	
- 利用brew安装ldid

		$ brew install ldid





### 二、修改环境变量

- 编辑⽤用户的配置⽂文件

   $ vim ~/.bash_profile 

- 在.bash_profie⽂件后面加⼊以下2⾏

  ```shell
  export THEOS=~/theos
  export PATH=$THEOS/bin:$PATH
  ```

- 让.bash_profiel配置的环境变量立即生效(或者重新打开终端)

   ```shell
   $ source ~/.bash_profile
   ```





### 三、下载theos

- 建议在$THEOS目录下载代码(也就是刚才配置的~/theos目录)

   ```shell
   git clone --recursive https://github.com/theos/theos.git $THEOS
   ```

   ​	



### 四、新建tweak项⽬目

- cd到⼀一个存放项⽬代码的⽂件夹(比如桌⾯面)

  ```shell
  $ cd ~/Desktop
  $ nic.pl
  ```

- 选择 **[11.] iphone/tweak**

  ![Alt text](./assets/imgs/07-Theos/1.png "Optional title")

- 填写项目信息
  - Project Name
  	- 项⽬名称 
  	
  - Package Name
  	- 项⽬ID(随便便写，**不能有下划线**)

  - Author/Maintainer Name
  	- 作者
  	- 直接敲回车按照默认做法就行(默认是Mac上的用户名)

  - [iphone/tweak] MobileSubstrate Bundle filter
  	- 需要修改的APP的Bundle Identifier(喜⻢拉雅FM的是com.gemd.iting)
  	- 可以通过Cycript查看APP的Bundle Identifier
  	
  - [iphone/tweak] List of applications to terminate upon installation
  	- 直接敲回车按照默认做法就行

  ```shell
  Project Name (required): ting_tweak
  Package Name [com.yourcompany.ting_tweak]: com.mj.ting
  Author/Maintainer Name [MJ Lee]:
  [iphone/tweak] MobileSubstrate Bundle filter [com.apple.springboard]:
  com.gemd.iting
  [iphone/tweak] List of applications to terminate upon installation (space-
  separated, '-' for none) [SpringBoard]:
  Instantiating iphone/tweak in ting_tweak/...
  Done.
  ```





### 五、编辑Makefile

- 在前⾯加入环境变量，写清楚通过哪个IP和端口访问⼿机
	- THEOS_DEVICE_IP
	- THEOS_DEVICE_PORT

	```	 shell
	export THEOS_DEVICE_IP=127.0.0.1
	export THEOS_DEVICE_PORT=10010
	
	include $(THEOS)/makefiles/common.mk
	
	TWEAK_NAME = ting_tweak
	ting_tweak_FILES = Tweak.xm
	
	include $(THEOS_MAKE_PATH)/tweak.mk
	
	after-install::
   		install.exec "killall -9 SpringBoard"
	```
	
- 如果不希望每个项⽬的Makefile都编写IP和端⼝环境变量，也可以添加到⽤户配置文件中
	- 编辑完毕后，$ source ~/.bash_profile让配置⽣效(或者重启终端)

	``` shell
	$ vim ~/.bash_profile
	
	export THEOS=~/theos
	export PATH=$THEOS/bin:$PATH
	export THEOS_DEVICE_IP=127.0.0.1
	export THEOS_DEVICE_PORT=10010
	
	$ source ~/.bash_profile
	```
  





### 六、编写代码

- 打开Tweak.xm⽂文件

  ```	 objective-c
  %hook XMAdAnimationView
  
  - (id)initWithImageUrl:(id)arg1
                   title:(id)arg2
                iconType:(long long)arg3
                jumpType:(long long)arg4 {
  	return nil; 
  }
  
  %end
  
  %hook XMSoundPatchPosterView
  
  - (id)initWithFrame:(struct CGRect)arg1 {
  return nil;
  }
  
  %end
  ```





### 七、编译-打包-安装

**特别注意：编译文件的路径不能在有中文的路径下，否这不能通过编译**

- 编译

		make
	
- 打包成deb
	
		make package
	
- 安装（默认会自动重启SpringBoard)

		make install
	​	



### 八、可能遇到的问题

#### 1. make package的错误

​		 

```shell
$ make package

Can't locate IO/Compress/Lzma.pm in @INC (you may need to install the
IO::Compress::Lzma module) (@INC contains: /Library/Perl/5.18/darwin-
thread-multi-2level /Library/Perl/5.18 /Network/Library/Perl/5.18/darwin-
thread-multi-2level /Network/Library/Perl/5.18 /Library/Perl/Updates/5.18.2
/System/Library/Perl/5.18/darwin-thread-multi-2level
/System/Library/Perl/5.18 /System/Library/Perl/Extras/5.18/darwin-thread-
multi-2level /System/Library/Perl/Extras/5.18 .) at
/Users/mj/theos/bin/dm.pl line 12.
BEGIN failed--compilation aborted at /Users/mj/theos/bin/dm.pl line 12.
make: *** [internal-package] Error 2
```

- 是因为打包压缩⽅式有问题，改成gzip压缩就⾏
  - 修改dm.pl⽂件，⽤#号注释掉下⾯面两句

     ```shell
     $ vim $THEOS/vendor/dm.pl/dm.pl
     ```

     ```shell
     #use IO::Compress::Lzma;
     #use IO::Compress::Xz;
     ```

   - 修改deb.mk文件第6行的压缩⽅方式为gzip

    ```shell
    $ vim $THEOS/makefiles/package/deb.mk
    ```

    ```shell
    _THEOS_PLATFORM_DPKG_DEB_COMPRESSION ?= gzip
    ```


#### 2. make的错误

```shell
$ make

Error: You do not have an SDK in /Library/Developer/CommandLineTools/Platforms/iPhoneOS.platform/Developer/SDKs
```

- 是因为多个xcode导致路径(有可能安装了好几个Xcode)，需要指定一下Xcode

    ```shell
    $ sudo xcode-select --switch /Applications/Xcode.app/Contents/Developer/
    ```

    ​	

    ``` 
    $ make
    > Making all for tweak xxx...
    make[2]: Nothing to be done for `internal-library-compile'.
    ```

    - 是因为之前已经编译过，有缓存导致的，clean⼀下即可

       ```shell
       $ make clean
       $ make
       ```


### 九、练习

- 将桌⾯面的更新数字去掉
 ![Alt text](./assets/imgs/07-Theos/2.png "Optional title")
	​	
- 给微信的“发现”界⾯增加2⾏功能
![Alt text](./assets/imgs/07-Theos/3.png "Optional title")
	​	

- 去掉内涵段⼦的⼴告
	- ⾸页的广告
	![Alt text](./assets/imgs/07-Theos/4.png "Optional title")
	- 评论区的广告
	![Alt text](./assets/imgs/07-Theos/5.png "Optional title")







### ⼗、theos资料料查询

- 目录结构:<https://github.com/theos/theos/wiki/Structure>

- 环境变量量:<http://iphonedevwiki.net/index.php/Theos>

- Logos语法:<http://iphonedevwiki.net/index.php/Logos>

  - **%hook、%end**：hook 一个类的开始和结束

  - **%log**：打印⽅方法调⽤用详情

    - 可以通过Xcode -> Window -> Devices and Simulators查看日志

  - **HBDebugLog**：跟NSLog类似

  - **%new**：添加一个新的⽅方法

  - **%c(className)**：⽣成⼀个Class对象，⽐如%c(NSObject)，类似于
    NSStringFromClass()、objc_getClass() 

  - **%orig**：函数原来的代码逻辑 

  - **%ctor**：在加载动态库时调用

  - **%dtor**：在程序退出时调用

     ![Alt text](./assets/imgs/07-Theos/6.png "Optional title")

  - **logify.pl**：可以将⼀个头文件快速转换成已经包含打印信息的xm文件

     ```shell
     # 没有文件，就创建，文件存在就覆盖之前的内容
     $ logify.pl xx.h > xx.xm
     
     # 两个[>>] 表示往 xx.xm 文件中追加内容
     $ logify.pl xx.h >> xx.xm
     ```

- 如果有额外的资源文件(比如图⽚)，只需放在编写的**tweak**项⽬目录的**layout** （默认没有该文件夹，需要自己创建）文件夹中，对应着⼿机的 **/**  根路径

- 为了方便和其他的插件区分，不应该放在 **/** 根路径，应该放在 **/Library/PreferenceLoader/Preferences/自己的插件文件名** 文件夹中

```objective-c
// 资源文件宏定义
// 方式一(标准)
#define SLFile(path) [@"/Library/PreferenceLoader/Preferences/SLWeChat/" stringByAppendingPathComponent:path]

// 方式二(优化)
// NSString初始化的一种写法(编译器会自动把多行字符串拼接成一行)
NSString *path = @"/Library/PreferenceLoader"
    			 "/Preferences/SLWeChat/"
    			 "xxx.png";
// # 字符是 宏定义 的语法，只要给传进来的 字符串 前面添加一个 # 字符，就会自动给该 字符串 添加双引号
#define SLFile(path) @"/Library/PreferenceLoader/Preferences/SLWeChat/" #path
```


- 如果觉得文件名太长，也可以放在下面这个文件夹

  ```c
  #define SLFile(path) @"/Library/Caches/SLWeChat/" #path 
  ```

- 多文件开发

  - 可以在tweak目录下，创建多个或多层文件夹

  - 修改 **Makefile** 文件

    ```makefile
    # 需要参与编译的代码文件
    # 方式1.以空格分开，通配符：支持文件名，不支持文件夹，文件格式支持 .h .m
    tweak_wechat_FILES = src/Tweak.xm src/Model/*m src/View/*.m src/Controller/*m
    
    # 方式2.通配符简化方式
    tweak_wechat_FILES = $(wildcard src/*xm)
    ```

    ![Alt text](./assets/imgs/07-Theos/7.png "Optional title")

  ​	

  - 使用其他文件的代码

    ```objective-c
    // 指定目标具体路径，不能使用通配符
    #import "src/Model/Person.h"
    ```

- 发布Release版本

  ```
  // 指定参数
  make package debug=0
  ```




- 命令行快捷方式处理：创建shell脚本文件，存放到Mac电脑的 **~** 路径文件夹

  ```shell
  $ cd ~
  $ touch tweak.sh
  $ vim tewak.sh
  # 输入 i 修改 tweak.sh
  i
  
  # tweak.sh 内容 make package[打包] 指令已经包含 make[编译]
  make clean && make package debug=0 && make install
  
  # 按Esc退出输入
  
  # 保存并退出
  :wq
  
  # 执行shell
  sh ~/tweak.sh
  
  ```


### ⼗一、theos-tweak的实现过程

- 编写Tweak代码
- **$ make**：编译Tweak代码为动态库(*.dylib)
- **$ make package**：将dylib打包为deb⽂文件
- **$ make install**：将deb文件传送到手机上，通过Cydia安装deb 
- 插件将会安装在**/Library/MobileSubstrate/DynamicLibraries**⽂件夹中
  - ***.dylib**:编译后的Tweak代码
  - ***.plist**:存放着需要hook的APP ID 
- 卸载插件
  - 使用iFunxBox选择**/Library/MobileSubstrate/DynamicLibraries**文件夹选中上面两个文件删除
  - 使用Ciydia App —>选择已安装的插件—> 卸载【推荐，干净卸载，无残留文件】
- 当打开APP时
  - Cydia Substrate(Cydia已⾃动安装的插件)会让APP去加载对应的dylib
  - 修改APP内存中的代码逻辑，去执行dylib中的函数代码 
  - 所以，theos的tweak并不会对APP原来的可执⾏文件进⾏修改，仅仅是修改了内存中的代码逻辑
- 疑问
  - 未脱壳的APP是否支持tweak?
    - 支持，因为tweak是在内存中实现的，并没有修改.app包中的可执行⽂件 

  - tweak效果是否永久性的?
    - 取决于tweak中用到的APP代码是否被修改过

  - 如果⼀旦更新APP，tweak会不会失效?
    - 取决于tweak中用到的APP代码是否被修改过 

  - 未越狱的⼿机是否⽀支持tweak?
    - 不支持

  - 能不能对Swift\C函数进行tweak?
    - 可以，⽅式跟OC不一样 

  - 能不能对游戏项⽬进行tweak?
    - 可以 
    - 但是游戏大多数是通过C++\C#编写的，⽽且类名、函数名会进行混淆操作





### 十二、theos - tweak的开发过程

![Alt text](./assets/imgs/07-Theos/8.png "Optional title")

![Alt text](./assets/imgs/07-Theos/9.png "Optional title")







### ⼗三、logify.pl注意点

- **logify.pl**⽣成的**xm**⽂件，有很多时候是**编译不通过**的，需要进行一些处理 

  - 删掉**__weak**

  - 删掉**inou**t 

  - 删掉协议，比如<XXTestDelegate>

    - 或者声明⼀下协议信息**@protocol XXTestDelegate;**

  - 删掉**- (void).cxx_destruct { %log; %orig; }**

  - 对象无法转诊替换**HBLogDebug(@" = 0x%x", (unsigned int)r);为HBLogDebug(@" = 0x%@", r);**  

  - 替换类名为**void**，⽐如将  **XXPerson** * 替换为 **void *** 

    - 或者声明一下类信息 **@class XXPerson**;

  - 只打印方法名调试

    - 使用Sublime Text打开xxx.xm
    - 快捷键 **command + f** 搜索 **%log;**
    - 快捷键 **command + shift + f** 替换 Replace: **NSLog(@"%@", NSStringFormSelector(_cmd));**

    ![Alt text](./assets/imgs/07-Theos/10.png "Optional title")

### 十四、%new注意点

- 如果是直接调用新增的方法，需要在@interface中声明

  ```objective-c
  // Tweak.xm
  
  // SLMethodSet 名字可以随意取
  @interface SLMethodSet
  - (void)test;
  @end
      
  %hook ViewController
  
  %new
  - (void)test {
      NSLog(@"%s", __func__);
  }
  
  - (id)touchesBegan:(id)arg1 withEvent:(id)agr2 {
      [self test];
  }
  
  %end
  ```






### 十五、MJAppTools

- 下载 <https://github.com/CoderMJLee/MJAppTools>

  ```shell
  git clone https://github.com/CoderMJLee/MJAppTools.git
  ```

- 使用方法
  - 使用Xcode打开，选择越狱的真机，s编译运行，查看控制台输出
  - 执行 **make** 命令行，得到一个**执行文件**，拷贝到iPhone /usr/bin/
  - 修改权限

  	```shell
  	chmod +x /usr/bin/MJAppTools
  	```
  - 执行命令

  	```shell
  	# 没有添加参数
  	iPhone:~ root# MJAppTools
  	 		-l  <regex>	列出用户安装的应用
  			-le <regex>	列出用户安装的加壳应用
  			-ld <regex>	列出用户安装的未加壳应用
  			-ls <regex>	列出系统的应用
  	```
  	
  	```shell
  	# 列出用户安装的应用
  	iPhone:~ root# MJAppTools -l
  	```
  	
  	```shell
  	# 支持 grep 过滤搜索
  	iPhone:~ root# MJAppTools -l | grep 微信
  	```
  	
  	```shell
  	# 支持 正则 搜索
  	iPhone:~ root# MJAppTools -l 视频
  	iPhone:~ root# MJAppTools -l tencent
  	```