[TOC]



## KVO

> KVO的全称是Key-Value Observing，俗称“键值监听”，可以用于监听某个对象属性值的改变

![01](./assets/imgs/15-KVO/01.png)



### 未使用KVO监听的对象

![02](./assets/imgs/15-KVO/02.png)

### 使用了KVO监听的对象

![03](./assets/imgs/15-KVO/03.png)



### 查看_NSSet*AndNotify的存在

![04](./assets/imgs/15-KVO/04.png)



### _NSSet*ValueAndNotify的内部实现

![05](./assets/imgs/15-KVO/05.png)

- 调用`willChangeValueForKey:`
- 调用原来的setter实现
- 调用`didChangeValueForKey:`
  - 内部会调用observer的`observeValueForKeyPath:ofObject:change:context:`方法



### 面试题

#### 1、iOS用什么方式实现对一个对象的KVO？(KVO的本质是什么？)

- 利用RuntimeAPI动态生成一个子类，并且让instance对象的isa指向这个全新的子类
- 当修改instance对象的属性时，会调用Foundation的_NSSetXXXValueAndNotify函数
  - willChangeValueForKey:
  - 父类原来的setter
  - didChangeValueForKey:

    - 内部会触发监听器（Oberser）的监听方法

      ` observeValueForKeyPath:ofObject:change:context:`



#### 2、如何手动触发KVO？

- 手动调用`willChangeValueForKey:`和`didChangeValueForKey:`



#### 3、直接修改成员变量会触发KVO么？

- 不会触发KVO