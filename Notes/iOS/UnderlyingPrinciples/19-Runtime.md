[TOC]

## Runtime

> Objective-C是一门动态性比较强的编程语言，跟C、C++等语言有着很大的不同
>
> Objective-C的动态性是由Runtime API来支撑的
>
> Runtime API提供的接口基本都是C语言的，源码由C\C++\汇编语言编写



### 共用体 - union

#### 1. API调用

```objective-c
// main.m

#import <Foundation/Foundation.h>
#import "Person.h"
    
int main(int argc, const char * argv[]) {
    @autoreleasepool {
        Person *person = [[Person alloc] init];
        person.tall = NO;
        person.rich = YES;
        person.handsome = NO;
        
        NSLog(@"tall:%d rich:%d hansome:%d", person.isTall, person.isRich, person.isHandsome);
    }
    return 0;
}
```



#### 2. 设计API的一般方式

平时开发，我们设计API的一般方式如下

```objective-c
// Person.h
#import <Foundation/Foundation.h>

@interface Person : NSObject

// 编译器特性，会默认生成 _name（成员变量），setter（方法），getter（方法）
@property (assign, nonatomic, getter=isTall) BOOL tall;
@property (assign, nonatomic, getter=isRich) BOOL rich;
@property (assign, nonatomic, getter=isHansome) BOOL handsome;

@end
   
// Person.m
@implementation Person
    
@end
```



#### 3. 掩码优化（节省内存分配）

观察上面代码，可以进一步优化，通过掩码（节省内存分配）

```objective-c
// Person.h

#import <Foundation/Foundation.h>

@interface Person : NSObject
    
// 编译器特性，会默认生成 _name（成员变量），setter（方法），getter（方法）
// @property (assign, nonatomic, getter=isTall) BOOL tall;
// @property (assign, nonatomic, getter=isRich) BOOL rich;
// @property (assign, nonatomic, getter=isHansome) BOOL handsome;

// 自定义实现setter（方法）
- (void)setTall:(BOOL)tall;
- (void)setRich:(BOOL)rich;
- (void)setHandsome:(BOOL)handsome;

// 自定义实现getter（方法）
- (BOOL)isTall;
- (BOOL)isRich;
- (BOOL)isHandsome;

@end
```



```objective-c
// Person.m

#import "Person.h"

// 掩码，一般用来按位与(&)运算的
//#define TallMask 1
//#define RichMask 2
//#define HandsomeMask 4

//#define TallMask 0b00000001
//#define RichMask 0b00000010
//#define HandsomeMask 0b00000100

#define TallMask (1 << 0)
#define RichMask (1 << 1)
#define HandsomeMask (1 << 2)

@interface Person() {
    char _tallRichHansome;
}
@end
    
@implementation Person
 
- (void)setTall:(BOOL)tall {
    if (tall) {
        _tallRichHansome |= TallMask;
    } else {
        _tallRichHansome &= ~TallMask; // 掩码取反，然后按位与
    }
}

- (BOOL)isTall {
    /**
     将char类型转成BOOL类型
     char: 0b1 (1个字节)
     BOOL: 0b0001 (4个字节)
     1个字节 0b1 -> BOOL == 0b1111 == 255 == -1(有符号) 
     2个字节 0b01 -> BOOL == 0b0001 == 1
     两次取反操作可以: -1 != YES -> !-1 == NO -> !!-1 == YES
     */
    return !!(_tallRichHansome & TallMask); 
}

- (void)setRich:(BOOL)rich {
    if (rich) {
        _tallRichHansome |= RichMask;
    } else {
        _tallRichHansome &= ~RichMask;
    }
}

- (BOOL)isRich {
    return !!(_tallRichHansome & RichMask); 
}

- (void)setHandsome:(BOOL)handsome {
    if (handsome) {
        _tallRichHansome |= HandsomeMask;
    } else {
        _tallRichHansome &= ~HandsomeMask;
    }
}

- (BOOL)isHandsome {
    return !!(_tallRichHansome & HandsomeMask);
}
    
@end
```



#### 4. 位域优化

```objective-c
// Person.m

#import "Person.h"

@interface Person() {
    // 结构体支持 位域 技术
    struct {
        char tall : 1;  // 使用的 : 1，表示tall只占1位，忽略前面的 char 类型
        char rich : 1;
        char handsome : 1;
    } _tallRichHandsome; // 表示该结构体中tall, rich, handsome各占1位，结构体只占用一个字节（8位），其中只用到了上面 3位 用来做事情 
}
@end

@implementation Person

- (void)setTall:(BOOL)tall {
    _tallRichHandsome.tall = tall;
}

- (BOOL)isTall {
    return !!_tallRichHandsome.tall;
}

- (void)setRich:(BOOL)rich {
    _tallRichHandsome.rich = rich;
}

- (BOOL)isRich {
    return !!_tallRichHandsome.rich;
}

- (void)setHandsome:(BOOL)handsome {
    _tallRichHandsome.handsome = handsome;
}

- (BOOL)isHandsome {
    return !!_tallRichHandsome.handsome;
}

@end
```



#### 5. 共同体优化

> 1.节省内存分配
>
> 2.增加代码可读性
>
> 3.便于代码维护，列：新增API接口

```objective-c
// Person.h

#import <Foundation/Foundation.h>

@interface Person : NSObject

- (void)setTall:(BOOL)tall;
- (void)setRich:(BOOL)rich;
- (void)setHandsome:(BOOL)handsome;
- (void)setThin:(BOOL)thin; // new API

- (BOOL)isTall;
- (BOOL)isRich;
- (BOOL)isHandsome;
- (BOOL)isThin; // new API

@end

#define TallMask (1<<0)
#define RichMask (1<<1)
#define HandsomeMask (1<<2)
#define ThinMask (1<<3) 

@interface Person() {
    // 定义共用体
    union { 
        char bits; 
        /** 结构体只为了增加代码可读性，写不写都不影响共用体的结构 */
        struct {
            char tall : 1;
            char rich : 1;
            char handsome : 1;
            char thin : 1; // new property
        };
    } _tallRichHandsome;
}
@end

// Person.m
@implementation Person

- (void)setTall:(BOOL)tall {
    if (tall) {
        _tallRichHandsome.bits |= TallMask;
    } else {
        _tallRichHandsome.bits &= ~TallMask; 
    }
}

- (BOOL)isTall {
    return !!(_tallRichHandsome.bits & TallMask);
}

- (void)setRich:(BOOL)rich {
    if (rich) {
        _tallRichHandsome.bits |= RichMask;
    } else {
        _tallRichHandsome.bits &= ~RichMask;
    }
}

- (BOOL)isRich {
    return !!(_tallRichHandsome.bits & RichMask);
}

- (void)setHandsome:(BOOL)handsome {
    if (handsome) {
        _tallRichHandsome.bits |= HandsomeMask;
    } else {
        _tallRichHandsome.bits &= ~HandsomeMask;
    }
}

- (BOOL)isHandsome {
    return !!(_tallRichHandsome.bits & HandsomeMask);
}

- (void)setThin:(BOOL)thin { // new
    if (thin) {
        _tallRichHandsome.bits |= ThinMask;
    } else {
        _tallRichHandsome.bits &= ~ThinMask;
    }
}

- (BOOL)isThin { // new
    return !!(_tallRichHandsome.bits & ThinMask);
}

// main.m
int main(int argc, const char * argv[]) {
    @autoreleasepool {
        Person *person = [[Person alloc] init];
        preson.tall = YES;
        person.rich = YES;
        person.handsome = NO;
        person.thin = YES; // new
        
        NSLog(@"tall:%d rich:%d hansome:%d thin:%d", 
              person.isTall,
              person.isRich,
              person.isHandsome, 
              person.isThin); 
    }
    return 0;
}
```



### isa详解

> 要想学习Runtime，首先要了解它底层的一些常用数据结构，比如isa指针
>
> 在arm64架构之前，isa就是一个普通的指针，存储着Class、Meta-Class对象的内存地址
>
> 从arm64架构开始，对isa进行了优化，变成了一个共用体（union）结构，还使用位域来存储更多的信息
>
> 将一个64位的内存数据分开来存储很多东西，而其中的33位才是存储着Class、Meta-Class对象的内存地址

![01](./assets/imgs/19-Runtime/01.png)



#### 位域

- nonpointer
  - 0，代表普通的指针，存储着Class、Meta-Class对象的内存地址
  - 1，代表优化过，使用位域存储更多的信息

- has_assoc

  - 是否有设置过关联对象，如果没有，释放时会更快

- has_cxx_dtor

  - 是否有C++的析构函数（.cxx_destruct），如果没有，释放时会更快

- shiftcls

  - 存储着Class、Meta-Class对象的内存地址信息

- magic

  - 用于在调试时分辨对象是否未完成初始化

- weakly_referenced

  - 是否有被弱引用指向过，如果没有，释放时会更快

- deallocating

  - 对象是否正在释放

- extra_rc

  - 里面存储的值是引用计数器减1

- has_sidetable_rc
  - 引用计数器是否过大无法存储在isa中
  - 如果为1，那么引用计数会存储在一个叫SideTable的类的属性中



### Class的结构

![02](./assets/imgs/19-Runtime/02.png)

1. bits 最开始指向的是 class_ro_t（包含了类的初始内容）
2. 通过Runtime加载分类的内容
3. 将 class_ro_t（包含了类的初始内容）和 分类的内容，合并到 class_rw_t
4. 最后更改 bits 的指向 class_rw_t



#### class_rw_t

> class_rw_t里面的methods、properties、protocols是二维数组，是可读可写的，包含了类的初始内容、分类的内容

![03](./assets/imgs/19-Runtime/03.png)



#### class_ro_t

> class_ro_t里面的baseMethodList、baseProtocols、ivars、baseProperties是一维数组，是只读的，包含了类的初始内容

![04](./assets/imgs/19-Runtime/04.png)



#### method_t

> method_t是对方法\函数的封装

```c++
struct method_t {
    SEL name;			// 函数名
    const char *types;	// 编码（返回值类型、参数类型）
    IMP imp;			// 指向函数的指针（函数地址）
}
```




##### SEL

> 代表方法\函数名，一般叫做选择器，底层结构跟char *类似

- 可以通过@selector()和sel_registerName()获得
- 可以通过sel_getName()和NSStringFromSelector()转成字符串
- 不同类中相同名字的方法，所对应的方法选择器是相同的

```c++
typedef struct objc_selector *SEL;
```



##### types

> 包含了函数返回值、参数编码的字符串

![05](./assets/imgs/19-Runtime/05.png)




##### IMP

> 代表函数的具体实现

```c++
typedef id _Nullable (*IMP)(id _Nonnull, SEL _Nonnull, ...);
```



#### Type Encoding

> iOS中提供了一个叫做@encode的指令，可以将具体的类型表示成字符串编码

![06](./assets/imgs/19-Runtime/06.png)

```objective-c
// 打印类型
// %s输出c语言字符串
NSLog(@“%s”, @encode(SEL)); // :
NSLog(@“%s”, @encode(id));  // @

// Person.h
#import <Foundation/Foundation.h>

@interface Person : NSObject
     
/**
 "i24@0:8i16f20"
 i == 返回值 int 类型   24 == 所有形参总字节大小
 @ == 参数1 (id)self    0 == 从第0个字节开始   id 占 8 个字节
 : == 参数2 (SEL)_cmd   8 == 从第8个字节开始   SEL 是一个指针 占 8 个字节
 i == 参数3 (int)age   16 == 从第16个字节开始
 f == 参数4 (float)age 20 == 从第20个字节开始
 */
- (int)test:(int)age height:(float)height;
@end
```



#### 方法缓存 - cache_t

> Class内部结构中有个方法缓存（cache_t），用散列表（哈希表）来缓存曾经调用过的方法，可以提高方法的查找速度

![07](./assets/imgs/19-Runtime/07.png)

缓存查找

- objc-cache.mm
- bucket_t * cache_t::find(cache_key_t k, id receiver)



调用方法\函数的过程

1. 通过isa找到类\元类
2. 从方法缓存（cache_t）寻找曾经调用过的方法，如果没有找到
3. bits  & FAST_DATA_MASK → 找到 class_rw_t
4. 从 class_rw_t 中 methonds 中寻找调用方法
5. 如果找到方法就调用该方法，然后把调用该方法缓存到自己的 方法缓存（cache_t） 中，下次重复调用该方法就会直接去 执行第2步
6. 如果没有找到，就会触发一个经典的错误   **unrecognized selector sent to instance**



### objc_msgSend

> OC中的方法调用，其实都是转换为objc_msgSend函数的调用



#### 执行流程

objc_msgSend的执行流程可以分为3大阶段

1. 消息发送
2. 动态方法解析
3. 消息转发



#### 源码跟读

1. objc-msg-arm64.s
   - ENTRY _objc_msgSend
   - b.le	LNilOrTagged
   - CacheLookup NORMAL
   - .macro CacheLookup
   - .macro CheckMiss
   - STATIC_ENTRY __objc_msgSend_uncached
   - .macro MethodTableLookup
   - __class_lookupMethodAndLoadCache3

2. objc-runtime-new.mm
   - _class_lookupMethodAndLoadCache3

   - lookUpImpOrForward

   - getMethodNoSuper_nolock、search_method_list、log_and_fill_cache

   - cache_getImp、log_and_fill_cache、getMethodNoSuper_nolock、log_and_fill_cache

   - _class_resolveInstanceMethod

   - _objc_msgForward_impcache

3. objc-msg-arm64.s
   - STATIC_ENTRY __objc_msgForward_impcache

   - ENTRY __objc_msgForward

4. Core Foundation

   - __ forwarding__（不开源）



#### 01-消息发送

![08](./assets/imgs/19-Runtime/08.png)

- 如果是从class_rw_t中查找方法
  - 已经排序的，二分查找
  - 没有排序的，遍历查找



- receiver通过isa指针找到receiverClass
- receiverClass通过superclass指针找到superClass



#### 02-动态方法解析

![09](./assets/imgs/19-Runtime/09.png)

- 开发者可以实现以下方法，来动态添加方法实现
  - +resolveInstanceMethod:
  - +resolveClassMethod:



- 动态解析过后，会重新走“消息发送”的流程
  - “从receiverClass的cache中查找方法”这一步开始执行



##### 动态添加方法

添加 OC 对象方法和类方法

```objective-c
// OC对象方法
- (void)other {
    NSLog(@"%s", __func__);
}

// 动态添加了对象方法
+ (BOOL)resolveInstanceMethod:(SEL)sel {
    if (sel == @selector(test)) {
        // 获取其他方法 Method可以理解为等价于struct method_t *
        Method method = class_getInstanceMethod(self, @selector(other));

        // 动态添加other方法的实现
        class_addMethod(self, // 类
                        sel,
                        method_getImplementation(method),
                        method_getTypeEncoding(method));

        // 返回YES代表有动态添加方法
        return YES;
    }
    return [super resolveInstanceMethod:sel];
}

// OC对象方法
+ (void)other {
    NSLog(@"%s", __func__);
}

// 动态添加了类方法
+ (BOOL)resolveClassMethod:(SEL)sel {
    if (sel == @selector(test)) {
        // 获取其他方法
        Method method = class_getInstanceMethod(self, @selector(other));

        // 动态添加other方法的实现
        class_addMethod(object_getClass(self), // 元类
                        sel,
                        method_getImplementation(method),
                        method_getTypeEncoding(method));

        // 返回YES代表有动态添加方法
        return YES;
    }
    return [super resolveInstanceMethod:sel];
}
```



添加 C 函数

```objective-c
// C 函数
void c_other(id self, SEL _cmd) {
    NSLog(@"c_other - %@ - %@", self, NSStringFromSelector(_cmd));
}

// 动态添加了对象方法
+ (BOOL)resolveInstanceMethod:(SEL)sel {
    if (sel == @selector(c_other)) {
        // 动态添加c_other函数的实现 
        class_addMethod(self,  // 类
                        sel, 
                        (IMP)c_other, 
                        "v16@0:8");

        // 返回YES代表有动态添加方法
        return YES;
    }
    return [super resolveInstanceMethod:sel];
}

// 动态添加类方法
+ (BOOL)resolveClassMethod:(SEL)sel {
    if (sel == @selector(test)) {
        // 动态添加c_other函数的实现
        class_addMethod(object_getClass(self), // 元类
                        sel, 
                        (IMP)c_other, 
                        "v16@0:8");
        return YES;
    }
    return [super resolveClassMethod:sel];
}
```



##### @dynamic

> 是告诉编译器不用自动生成getter和setter的实现、不要自动生成成员变量，等到运行时再添加方法实现

```objective-c
#import <Foundation/Foundation.h>

@interface Person : NSObject
@property (assign, nonatomic) int age;
@end
    
@implementation Person
// 告诉编译器不要自动生成setter和getter的实现、不要自动生成成员变量 _age
@dynamic age;
@end
```



#### 03-消息转发

![10](./assets/imgs/19-Runtime/10.png)

- 开发者可以在forwardInvocation:方法中自定义任何逻辑
- 以上方法都有对象方法、类方法2个版本（前面可以是加号+，也可以是减号-）



##### 生成NSMethodSignature

```objective-c
#import <Foundation/Foundation.h>
#import <objc/runtime.h>

@interface Student : NSObject
- (int)test:(int)age;
@end
    
@implementation Student
- (int)test:(int)age {
    return age * 2;
}
@end
    
@interface Person : NSObject
- (int)test:(int)age;
@end
  
@implementation Person
// 如果转发的是类方法，实现 + (NSMethodSignature *)methodSignatureForSelector:(SEL)aSelector
- (NSMethodSignature *)methodSignatureForSelector:(SEL)aSelector {
    if (aSelector == @selector(test:)) {
        // 生成NSMethodSignature
		// return [NSMethodSignature signatureWithObjCTypes:"v20@0:8i16"];
        return [NSMethodSignature signatureWithObjCTypes:"i@:i"];
		// return [[[Student alloc] init] methodSignatureForSelector:aSelector];
    }
    return [super methodSignatureForSelector:aSelector];
}

// 如果转发的是类方法，实现 + (void)forwardInvocation:(NSInvocation *)anInvocation
- (void)forwardInvocation:(NSInvocation *)anInvocation {
	// 参数顺序：receiver、selector、other arguments
	// int age;
	// [anInvocation getArgument:&age atIndex:2];
	// NSLog(@"%d", age + 10);
    
    // anInvocation.target == [[Student alloc] init]
    // anInvocation.selector == test:
    // anInvocation的参数：15
    // [[[Student alloc] init] test:15]
    
    [anInvocation invokeWithTarget:[[Student alloc] init]];
    
    int ret;
    [anInvocation getReturnValue:&ret];
    
    NSLog(@"%d", ret);
}
@end
    
int main(int argc, const char * argv[]) {
    @autoreleasepool {
        Person *person = [[Person alloc] init];
        // 消息转发：将消息转发给别人
        [person test:15];
    }
    return 0;
}
```



### super的本质

> super调用，底层会转换为objc_msgSendSuper2函数的调用，接收2个参数

- struct objc_super2
- SEL

```objective-c
struct objc_super2 {
    id receiver;			// receiver是消息接收者
    Class current_class;	// current_class是receiver的Class对象
};
```



### LLVM的中间代码（IR）

> Objective-C在变为机器代码之前，会被LLVM编译器转换为中间代码（Intermediate Representation）



#### 可以使用以下命令行指令生成中间代码

`clang -emit-llvm -S main.m`



#### 语法简介

- @ - 全局变量
- % - 局部变量
- alloca - 在当前执行的函数的堆栈帧中分配内存，当该函数返回到其调用者时，将自动释放内存
- i32 - 32位4字节的整数
- align - 对齐
- load - 读出
- store - 写入
- icmp - 两个整数值比较，返回布尔值
- br - 选择分支，根据条件来转向label，不根据条件跳转的话类似 goto
- label - 代码标签
- call - 调用函数



#### 具体可以参考

官方文档：https://llvm.org/docs/LangRef.html



### Runtime的应用

#### 01 – 查看私有成员变量

设置UITextField占位文字的颜色

```objective-c
// 方式1.使用富文本设置
// NSMutableDictionary *attrs = [NSMutableDictionary dictionary];
// attrs[NSForegroundColorAttributeName] = [UIColor redColor];
// self.textField.attributedPlaceholder = [[NSMutableAttributedString alloc] initWithString:@"请输入用户名" attributes:attrs];


// 方式2.利用Runtime遍历所有的属性或者成员变量
unsigned int count;
Ivar *ivars = class_copyIvarList([UITextField class], &count);
for (int i = 0; i < count; i++) {
    // 取出i位置的成员变量
    Ivar ivar = ivars[i];
    NSLog(@"%s %s", ivar_getName(ivar), ivar_getTypeEncoding(ivar));
}
free(ivars);

// 然后利用KVC设值
self.textField.placeholder = @"请输入用户名";
[self.textField setValue:[UIColor redColor] forrKeyPath:@"_placeholderLabel.textColor"];
// UILabel *placeholderLabel = [self.textField valueForKeyPath:@"_placeholderLabel"];
// placeholderLabel.textColor = [UIColor redColor];
```



#### 02 – 字典转模型

```objective-c
#import <objc/runtime.h>

@interface NSObject (JSON)

+ (instancetype)sl_objectWithJSON:(NSDictionary *)JSON;

@end

@implementation NSObject (JSON)

+ (instancetype)sl_objectWithJSON:(NSDictionary *)JSON {
    id obj = [[self alloc] init];
    
    unsigned int count;
    Ivar *ivars = class_copyIvarList(self, &count);
    for (int i = 0; i < count; i++) {
        // 取出i位置的成员变量
        Ivar ivar = ivars[i];
        NSMutableString *name = [NSMutableString stringWithUTF8String:ivar_getName(ivar)];
        [name deleteCharactersInRange:NSMakeRange(0, 1)];
        
        // 设值
        id value = JSON[name];
        if ([name isEqualToString:@"ID"]) {
            value = JSON[@"id"];
        }
        [obj setValue:value forKey:name];
    }
    free(ivars);
    
    return obj;
}

@end
```



#### 03 - 替换方法实现

> 一般使用在替换系统\第三方框架的方法

- class_replaceMethod
- method_exchangeImplementations

1. 拦截所有 UIButton 的事件

```objective-c
#import <UIKit/UIKit.h>

@interface UIControl (Extension)
@end
    
#import <objc/runtime.h>

@implementation UIControl (Extension)

+ (void)load {
    // hook：钩子函数
    Method method1 = class_getInstanceMethod(self, 
                                             @selector(sendAction:to:forEvent:));
    Method method2 = class_getInstanceMethod(self, 
                                             @selector(sl_sendAction:to:forEvent:));
    method_exchangeImplementations(method1, method2);
}

- (void)sl_sendAction:(SEL)action to:(id)target forEvent:(UIEvent *)event {
    NSLog(@"%@-%@-%@", self, target, NSStringFromSelector(action));
    
    // 调用系统原来的实现
    [self sl_sendAction:action to:target forEvent:event];
    
//    [target performSelector:action];
    
//    if ([self isKindOfClass:[UIButton class]]) {
//        // 拦截了所有按钮的事件
//
//    }
}

@end
```



2. 处理程序崩溃

   2.1 NSMutableArray 插入 nil 值 

   ```objective-c
   #import <Foundation/Foundation.h>
   #import <objc/runtime.h>
   
   @interface NSMutableArray (Extension)
   @end
   
   @implementation NSMutableArray (Extension)
   
   + (void)load {
      static dispatch_once_t onceToken;
      dispatch_once(&onceToken, ^{
          // 类簇：NSString、NSArray、NSDictionary，真实类型是其他类型
          Class cls = NSClassFromString(@"__NSArrayM");
          Method method1 = class_getInstanceMethod(cls,
                                                   @selector(insertObject:atIndex:));
       Method method2 = class_getInstanceMethod(cls, 
                                                   @selector(sl_insertObject:atIndex:));
          method_exchangeImplementations(method1, method2);
      });
   }
   
   - (void)sl_insertObject:(id)anObject atIndex:(NSUInteger)index {
      if (anObject == nil) return;
      
      [self sl_insertObject:anObject atIndex:index];
   }
   
   @end
   ```
   2.2 NSMutableDictionary 存取 nil 值

   ```objective-c
   #import <Foundation/Foundation.h>
   #import <objc/runtime.h>
   
   @interface NSMutableDictionary (Extension)
   @end
       
   @implementation NSMutableDictionary (Extension)
   
   + (void)load {
       static dispatch_once_t onceToken;
       dispatch_once(&onceToken, ^{
           Class cls = NSClassFromString(@"__NSDictionaryM");
           Method method1 = class_getInstanceMethod(cls, 		
                                                  @selector(setObject:forKeyedSubscript:));
           Method method2 = class_getInstanceMethod(cls, @selector(sl_setObject:forKeyedSubscript:));
           method_exchangeImplementations(method1, method2);
           
           Class cls2 = NSClassFromString(@"__NSDictionaryI");
           Method method3 = class_getInstanceMethod(cls2, @selector(objectForKeyedSubscript:));
           Method method4 = class_getInstanceMethod(cls2, @selector(sl_objectForKeyedSubscript:));
           method_exchangeImplementations(method3, method4);
       });
   }
   
   - (void)sl_setObject:(id)obj forKeyedSubscript:(id<NSCopying>)key {
       if (!key) return;
       [self sl_setObject:obj forKeyedSubscript:key];
   }
   
   - (id)sl_objectForKeyedSubscript:(id)key{
       if (!key) return nil;
       return [self sl_objectForKeyedSubscript:key];
   }
   
   @end
   ```


​	

### Runtime API

#### 01 – 类

动态创建一个类（参数：父类，类名，额外的内存空间）

```c
Class objc_allocateClassPair(Class superclass, const char *name, size_t extraBytes)
```


注册一个类（要在类注册之前添加成员变量）
```c
void objc_registerClassPair(Class cls) 
```

销毁一个类

```c
void objc_disposeClassPair(Class cls)
```


获取isa指向的Class
```objective-c
Class object_getClass(id obj)
```
设置isa指向的Class
```objective-c
Class object_setClass(id obj, Class cls)
```
判断一个OC对象是否为Class
```objective-c
BOOL object_isClass(id obj)
```
判断一个Class是否为元类
```objective-c
BOOL class_isMetaClass(Class cls)
```
获取父类
```objective-c
Class class_getSuperclass(Class cls)
```



#### 02 – 成员变量

获取一个实例变量信息
```objective-c
Ivar class_getInstanceVariable(Class cls, const char *name)
```
拷贝实例变量列表（最后需要调用free释放）
```objective-c
Ivar *class_copyIvarList(Class cls, unsigned int *outCount)
```
设置和获取成员变量的值
```objc
void object_setIvar(id obj, Ivar ivar, id value)
id object_getIvar(id obj, Ivar ivar)
```
动态添加成员变量（已经注册的类是不能动态添加成员变量的）
```objective-c
BOOL class_addIvar(Class cls,
                   const char * name,
                   size_t size,
                   uint8_t alignment,
                   const char * types)
```
获取成员变量的相关信息
```objective-c
const char *ivar_getName(Ivar v)
const char *ivar_getTypeEncoding(Ivar v)
```



#### 03 – 属性

获取一个属性
```objective-c
objc_property_t class_getProperty(Class cls, const char *name)
```

拷贝属性列表（最后需要调用free释放）
```objective-c
objc_property_t *class_copyPropertyList(Class cls, unsigned int *outCount)
```

动态添加属性
```objective-c
BOOL class_addProperty(Class cls, const char *name, const objc_property_attribute_t *attributes, unsigned int attributeCount)
```

动态替换属性
```objective-c
void class_replaceProperty(Class cls, 
                           const char *name,
                           const objc_property_attribute_t *attributes,
                           unsigned int attributeCount)
```

获取属性的一些信息
```objective-c
const char *property_getName(objc_property_t property)
const char *property_getAttributes(objc_property_t property)
```



#### 04 – 方法

获得一个实例方法、类方法
```objective-c
Method class_getInstanceMethod(Class cls, SEL name)
Method class_getClassMethod(Class cls, SEL name)
```

方法实现相关操作
```objective-c
IMP class_getMethodImplementation(Class cls, SEL name) 
IMP method_setImplementation(Method m, IMP imp)
void method_exchangeImplementations(Method m1, Method m2) 
```

拷贝方法列表（最后需要调用free释放）
```objective-c
Method *class_copyMethodList(Class cls, unsigned int *outCount)
```

动态添加方法
```objective-c
BOOL class_addMethod(Class cls, SEL name, IMP imp, const char *types)
```

动态替换方法
```objective-c
IMP class_replaceMethod(Class cls, SEL name, IMP imp, const char *types)
```

获取方法的相关信息（带有copy的需要调用free去释放）
```objective-c
SEL method_getName(Method m)
IMP method_getImplementation(Method m)
const char *method_getTypeEncoding(Method m)
unsigned int method_getNumberOfArguments(Method m)
char *method_copyReturnType(Method m)
char *method_copyArgumentType(Method m, unsigned int index)
```


选择器相关
```objective-c
const char *sel_getName(SEL sel)
SEL sel_registerName(const char *str)
```

用block作为方法实现
```objective-c
IMP imp_implementationWithBlock(id block)
id imp_getBlock(IMP anImp)
BOOL imp_removeBlock(IMP anImp)
```



### 面试题

#### 1.讲一下 OC 的消息机制

- OC中的方法调用其实都是转成了objc_msgSend函数的调用，给receiver（方法调用者）发送了一条消息（selector方法名）
- objc_msgSend底层有3大阶段
  - 消息发送（当前类、父类中查找）、动态方法解析、消息转发



#### 2.消息转发机制流程

- 描述一下objc_msgSend的执行流程



#### 3.什么是Runtime？平时项目中有用过么？

- OC是一门动态性比较强的编程语言，允许很多操作推迟到程序运行时再进行
- OC的动态性就是由Runtime来支撑和实现的，Runtime是一套C语言的API，封装了很多动态性相关的函数
- 平时编写的OC代码，底层都是转换成了Runtime API进行调用
- 具体应用
  - 利用关联对象（AssociatedObject）给分类添加属性
  - 遍历类的所有成员变量（修改textfield的占位文字颜色、字典转模型、自动归档解档）
  - 交换方法实现（交换系统的方法）
  - 利用消息转发机制解决方法找不到的异常问题
  - ......



#### 4.打印结果分别是什么？

- super 和 superclass

```objective-c
@interface Person : NSObject
@end
    
@implementation Person
@end
    
@interface Student : Person
@end
    
@implementation Student

/*
 [super message]的底层实现
 1.消息接收者仍然是子类对象
 2.从父类开始查找方法的实现
 */
- (instancetype)init {
    if (self = [super init]) {
        NSLog(@"[self class] = %@", [self class]); // Student
        NSLog(@"[self superclass] = %@", [self superclass]); // Person

        NSLog(@"--------------------------------");

        // objc_msgSendSuper({self, [Person class]}, @selector(class));
        NSLog(@"[super class] = %@", [super class]); // Student
        NSLog(@"[super superclass] = %@", [super superclass]); // Person
    }
    return self;
}

@end
```



- isKindOfClass 和 isMemberOfClass

```objective-c
// 这句代码的方法调用者不管是哪个类（只要是NSObject体系下的），都返回YES
NSLog(@"%d", [[NSObject class] isKindOfClass:[NSObject class]]);	// 1
NSLog(@"%d", [[NSObject class] isMemberOfClass:[NSObject class]]);	// 0
NSLog(@"%d", [[Person class] isKindOfClass:[Person class]]);	// 0
NSLog(@"%d", [[Person class] isMemberOfClass:[Person class]]);	// 0

/* 
 分析
 [NSObject class] == NSObject 获取的是 类对象
 实例对象 isKindOfClass 类对象
 类对象 isKindOfClass 元类对象
 上面代码等价下面代码
 */
NSLog(@"%d", [NSObject isKindOfClass:[NSObject class]]);	// 1
NSLog(@"%d", [NSObject isMemberOfClass:[NSObject class]]);	// 0
NSLog(@"%d", [Person isKindOfClass:[Person class]]);	// 0
NSLog(@"%d", [Person isMemberOfClass:[Person class]]);	// 0

```

参考源码 **NSObject.mm** 



#### 5.以下代码能不能执行成功？如果可以，打印结果是什么？

```objective-c
@interface Person : NSObject
@property (copy, nonatomic) NSString *name;
- (void)print;
@end
    
@implementation Person
- (void)print {
    NSLog(@"my name is %@", self->_name);
}
@end
    
@implementation ViewController
- (void)viewDidLoad {
    [super viewDidLoad]; 
    
    id cls = [Person class]; 
    void *obj = &cls;
    [(__bridge id)obj print]; 
}

@end
```

解题分析

1. 内存布局

 ![11](./assets/imgs/19-Runtime/11.png)



2. 添加 NSObject 代码

```objective-c
- (void)viewDidLoad {
    [super viewDidLoad]; 
    
    NSObject *obj2 = [[NSObject alloc] init]; // new
    
    id cls = [Person class]; 
    void *obj = &cls;
    [(__bridge id)obj print]; // my name is <NSObject: 0x60c000019a70 >
}
```

![12](./assets/imgs/19-Runtime/12.png)



3. 添加 @"123"

```objective-c
- (void)viewDidLoad {
    [super viewDidLoad]; 
    
    NSObject *obj2 = [[NSObject alloc] init];
    NSString *test = @"123"; // new
    id cls = [Person class]; 
    void *obj = &cls;
    [(__bridge id)obj print]; // my name is 123
}
```

![13](./assets/imgs/19-Runtime/13.png)



4. print为什么能够调用成功？

```objective-c
- (void)viewDidLoad {
    [super viewDidLoad]; // 影响打印结果的关键代码
    
//   [super viewDidLoad]底层是转成 消息机制 原理 代码如下
//    struct abc = {
//        self, 
//        [ViewController class]
//    };
//    objc_msgSendSuper2(abc, sel_registerName("viewDidLoad")); // 消息机制
    
    id cls = [Person class];
    void *obj = &cls;
    // _name 是 跳过 isa 8个字节，开始打印
    [(__bridge id)obj print]; // my name is <ViewController: 0x7fc966d07f10> 
}
```

![14](./assets/imgs/19-Runtime/14.png)