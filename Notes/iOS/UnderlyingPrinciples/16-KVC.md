[TOC]

## KVC

> KVC的全称是Key-Value Coding，俗称“键值编码”，可以通过一个key来访问某个属性



### 常见的API

```objective-c
- (void)setValue:(id)value forKeyPath:(NSString *)keyPath;
- (void)setValue:(id)value forKey:(NSString *)key;
- (id)valueForKeyPath:(NSString *)keyPath;
- (id)valueForKey:(NSString *)key; 
```



### setValue:forKey:的原理

![01](./assets/imgs/16-KVC/01.png)

`accessInstanceVariablesDirectly`方法的默认返回值是YES



### valueForKey:的原理

![02](./assets/imgs/16-KVC/02.png)

### 面试题

#### 1、通过KVC修改成员属性会出发KVO吗？

- 会触发
- 内部手动调用的KVO
  - willChangeValueForKey
  - didChangeValueForKey



#### 2、KVC的赋值和取值过程是怎样的？原理是什么？

描述一下上图中的过程

- setValue:forKey:的原理
- valueForKey:的原理