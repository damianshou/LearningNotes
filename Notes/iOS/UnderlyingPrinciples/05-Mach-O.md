[TOC]

## 初识Mach-O

### APP从开发到安装到手机的过程

![Alt text](./assets/imgs/05-Mach-O/1.png "Optional title")
![Alt text](./assets/imgs/05-Mach-O/2.png "Optional title")
​		

### 逆向APP的思路

1. 界面分析
	- Cycript、Reveal

2. 代码分析
	- 对Mach-O文件的静态分析
	- MachOView、class-dump、Hopper Disassembler、ida等

3. 动态调试
	- 对运行中的APP进行代码调试
	- debugserver、LLDB

4. 代码编写
	- 注入代码到APP中
	- 必要时还可能需要重新签名、打包ipa





## class-dump 

- 顾名思义，它的作用就是把Mach-O文件的class信息给dump出来（把类信息给导出来），生成对应的.h头文件

- 官方地址：<http://stevenygard.com/projects/class-dump/>

- 下载完工具包后将class-dump文件复制到Mac的/usr/local/bin目录，这样在终端就能识别class-dump命令了

- 常用格式
	```class-dump  -H  Mach-O文件路径  -o  头文件存放目录```
	
	* -H表示要生成头文件
	* -o用于制定头文件的存放目录

### 代码的编译过程
![Alt text](./assets/imgs/05-Mach-O/3.png "Optional title")
![Alt text](./assets/imgs/05-Mach-O/4.png "Optional title")





## Hopper Disassembler 

- Hopper Disassmbler能够将Mach-O文件的机器语言代码反编译成汇编代码、OC伪代码或者Swift伪代码

- 常用快捷键

key      				| description
------------------- | -----------
Shift + Option + X  | 找出哪里引用了这个方法


## 动态库共享缓存（dyld shared cache）
- 从iOS3.1开始，为了提高性能，绝大部分的系统动态库文件都打包存放到了一个缓存文件中（dyld shared cache）
	* 缓存文件路径：(Frameworks的Mach-O)

	```
	/System/Library/Caches/com.apple.dyld/dyld_shared_cache_armX
	```
	
	* Frameworks 路径

	```
	cd /Applications/Xcode.app/Contents/Developer/Platforms/iPhoneOS.platform/Developer/SDKs/iPhoneOS.sdk/System/Library/Frameworks
	```



- dyld_shared_cache_armX的X代表ARM处理器指令集架构

```
v6
iPhone、iPhone3G
iPod Touch、iPod Touch2

v7
iPhone3GS、iPhone4、iPhone4S
iPad、iPad2、iPad3(The New iPad)
iPad mini
iPod Touch3G、iPod Touch4、iPod Touch5

v7s
iPhone5、iPhone5C
iPad4

arm64
iPhone5S、iPhone6、iPhone6 Plus、iPhone6S、iPhone6S Plus
iPhoneSE、iPhone7、iPhone7 Plus、iPhone8、iPhone8 Plus、iPhoneX
iPad5、iPad Air、iPad Air2、iPad Pro、iPad Pro2
iPad mini with Retina display、iPad mini3、iPad mini4
iPod Touch6
```

- 所有指令集原则上都是向下兼容的

- 动态库共享缓存一个非常明显的好处是节省内存

- 现在的ida、Hopper反编译工具都可以识别动态库共享缓存

![Alt text](./assets/imgs/05-Mach-O/5.png "Optional title")
![Alt text](./assets/imgs/05-Mach-O/6.png "Optional title")

### 动态库的加载

- 在Mac\iOS中，是使用了/usr/lib/dyld程序来加载动态库

	* dyld

		1. dynamic link editor，动态链接编辑器
		2. dynamic loader，动态加载器

- dyld源码 <https://open./Sources.apple.com/tarballs/dyld/>

### 从动态库共享缓存抽取动态库

- 可以使用dyld源码中的launch-cache/dsc_extractor.cpp

  1. 将#if 0前面的代码删除（包括#if 0），把最后面的#endif也删掉
  2. 编译dsc_extractor.cpp

  ```shell
  clang++ -o dsc_extractor dsc_extractor.cpp
  ```

  * 建议:

  	得到一个 dsc_extractor 可执行文件，拖入到 /usr/local/bin/ 文件中就可以在终端任意位置输入该命令行，否则需要cd 到当前文件夹：

  ```shell
  cd dsc_extractor所在文件夹 ./dsc_extractor dyld_shared_cache_armv7s armv7s
  ```

  3. 使用dsc_extractor


  ```shell
  # ./dsc_extractor 动态库共享缓存文件的路径  用于存放抽取结果的文件夹
  
  $ cd /Users/Anthony/Desktop/iOS底层原理3期/05-初识Mach-O课件/资源
  dsc_extractor dyld_shared_cache_arm64 arm64
  
  ```

![Alt text](./assets/imgs/05-Mach-O/7.png "Optional title")

## Mach-O

- Mach-O是Mach object的缩写，是Mac\iOS上用于存储程序、库的标准格式
- 属于Mach-O格式的文件类型有

  ![Alt text](./assets/imgs/05-Mach-O/8.png "Optional title")

- 可以在xnu源码中，查看到Mach-O格式的详细定义（<https://open./Sources.apple.com/tarballs/xnu/>）
 1. EXTERNAL_HEADERS/mach-o/fat.h
 2. EXTERNAL_HEADERS/mach-o/loader.h

### 常见的Mach-O文件类型

- MH_OBJECT
	* 目标文件（.o）
	* 静态库文件(.a），静态库其实就是N个.o合并在一起

- MH_EXECUTE：可执行文件
	* .app/xx

- MH_DYLIB：动态库文件
	* .dylib
	* .framework/xx

- MH_DYLINKER：动态链接编辑器
	* /usr/lib/dyld

- MH_DSYM：存储着二进制文件符号信息的文件
	* .dSYM/Contents/Re./Sourcess/DWARF/xx（常用于分析APP的崩溃信息）

### 在Xcode中查看target的Mach-O类型

![Alt text](./assets/imgs/05-Mach-O/9.png "Optional title")

### Mach-O的基本结构

- 官方描述
	<https://developer.apple.com/library/content/documentation/DeveloperTools/Conceptual/MachOTopics/0-Introduction/introduction.html>

![Alt text](./assets/imgs/05-Mach-O/10.png "Optional title")

- 一个Mach-O文件包含3个主要区域
	* Header 
	* 文件类型、目标架构类型等

- Load commands
	* 描述文件在虚拟内存中的逻辑结构、布局

- Raw segment data
	* 在Load commands中定义的Segment的原始数据

### 窥探Mach-O的结构

- 命令行工具
	* file：查看Mach-O的文件类型
	
		```
		file  Mach-O文件
		```

	* otool：查看Mach-O特定部分和段的内容
		
		```
		otool -f Mach-O文件
		```

	* lipo：常用于多架构Mach-O文件的处理
		1. 查看架构信息：

		   ```
		   lipo  -info  Mach-O文件
		   ```
		   
		2. 导出某种特定架构：

			```
			lipo  文件路径  -thin  架构类型  -output  输出文件路径
			```
			
		3. 合并多种架构：
			```
			lipo  文件路径1  文件路径2  -output  输出文件路径
			```

- GUI工具
	* MachOView（<https://github.com/gdbinit/MachOView>）
	* 使用XCode打开，运行。
	* 得到一个mac App，拖到应用程序文件夹
	* 使用方法：File -> Open -> 选中Mach-O文件

### Universal Binary（通用二进制文件）

- 通用二进制文件
	* 同时适用于多种架构的二进制文件
	* 包含了多种不同架构的独立的二进制文件

- 因为需要储存多种架构的代码，通用二进制文件通常比单一平台二进制的程序要大

- 由于两种架构有共同的一些资源，所以并不会达到单一版本的两倍之多

- 由于执行过程中，只调用一部分代码，运行起来也不需要额外的内存

- 因为文件比原来的要大，也被称为“胖二进制文件”（Fat Binary）

### dyld和Mach-O

- dyld用于加载以下类型的Mach-O文件
	* MH_EXECUTE

	* MH_DYLIB

	* MH_BUNDLE

- APP的可执行文件、动态库都是由dyld负责加载的




