[TOC]



## Cycript使用



### 一、基本介绍

Cycript是Objective-C++、ES6（JavaScript）、Java等语法的混合物

可以用来探索、修改、调试正在运行的Mac\iOS APP

官网： <http://www.cycript.org/>

文档： <http://www.cycript.org/manual/>
![Alt text](./assets/imgs/03-Cycript/1.png "Optional title")



通过Cydia安装Cycript，即可在iPhone上调试运行中的APP
![Alt text](./assets/imgs/03-Cycript/2.png "Optional title")



### 二、Cycript的开启和关闭

- 开启

```shell
cycript
cycript -p 进程ID
cycript -p 进程名称
```
![Alt text](./assets/imgs/03-Cycript/3.png "Optional title")

- 取消输入：Ctrl + C

- 退出：Ctrl + D

- 清屏：Command + R



### 三、ps命令

1. 安装adv-cmds
   ![Alt text](./assets/imgs/03-Cycript/4.png "Optional title")



2. ps命令是process status的缩写，使用ps命令可以列出系统当前的进程

```shell
# 列出所有的进程
ps -A
ps aux
```



3. 搜索关键词

```shell
ps –A | grep 关键词
```



4. 加载动态库

```shell
@import mjcript
MJLoadFramework("Photos")
```



### 四、常用语法

1. 打印应用

```shell
UIApp
[UIApplication sharedApplication]
```



2. 定义变量

```shell
var 变量名 = 变量值
```



3. 用内存地址获取对象

```shell
#内存地址
```

![Alt text](./assets/imgs/03-Cycript/5.png "Optional title")



4. 已加载的所有OC类

```shell
ObjectiveC.classes
```



5. 查看对象的所有成员变量

```shell
*对象
```

![Alt text](./assets/imgs/03-Cycript/6.png "Optional title")



6. 递归打印view的所有子控件（跟LLDB一样的函数）

```
view.recursiveDescription().toString()
```



7. 筛选出某种类型的对象

```shell
choose(UIViewController)
choose(UITableViewCell)
```



### 五、封装Cycript - .cy文件编写

1. 我们可以将常用的Cycript代码封装在一个.cy文件中
2. exports参数名固定，用于向外提供接口

![Alt text](./assets/imgs/03-Cycript/7.png "Optional title")



### 六、封装Cycript - 存放和使用.cy文件

1. 将.cy文件存放到/usr/lib/cycript0.9目录下

![Alt text](./assets/imgs/03-Cycript/8.png "Optional title")



2. 在Cycript中引用.cy文件，并使用它提供的接口

![Alt text](./assets/imgs/03-Cycript/9.png "Optional title")



### 七、Cycript库

- <https://github.com/CoderMJLee/mjcript>
- 具体用法参考mjcript.cy文件



### 八、利用python打印字符		

1. 查看版本

```shell
python
```



2. 打印字符

```shell
print u'\u767b\u5f55'

unicode('登录', 'UTF-8')
```



3. 退出

```shell
exit()
```

![Alt text](./assets/imgs/03-Cycript/10.png "Optional title")
​		
![Alt text](./assets/imgs/03-Cycript/11.png "Optional title")