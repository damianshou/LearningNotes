[TOC]

## Reveal

- Reveal是一款调试iOS程序UI界面的神器

- 官网：<https://revealapp.com>

- 下载：<https://revealapp.com/download/>
	* 建议下载至少Reveal4版本，支持USB连接调试，速度快。低版本的只能WiFi连接调试



## 调试环境配置

- iPhone上安装Reveal Loader
	* 软件源：http://apt.so/codermjlee
	* 不要安装其他源的版本，有可能不支持新版Reveal

	![Alt text](./assets/imgs/04-Reveal/1.png "Optional title")
	![Alt text](./assets/imgs/04-Reveal/2.png "Optional title")
	![Alt text](./assets/imgs/04-Reveal/3.png "Optional title")

- 安装完Reveal Loader后，打开【设置】，选择需要调试的APP

	![Alt text](./assets/imgs/04-Reveal/4.png "Optional title")
	![Alt text](./assets/imgs/04-Reveal/5.png "Optional title")
	![Alt text](./assets/imgs/04-Reveal/6.png "Optional title")

- 找到Mac的Reveal中的RevealServer文件，覆盖iPhone的/Library/RHRevealLoader/RevealServer文件
	![Alt text](./assets/imgs/04-Reveal/7.png "Optional title")
	![Alt text](./assets/imgs/04-Reveal/8.png "Optional title")
	![Alt text](./assets/imgs/04-Reveal/9.png "Optional title")

- 重启SpringBoard或者重启手机，可以在iPhone上输入终端命令
	* 重启SpringBoard：killall SpringBoard
	* 重启手机：reboot