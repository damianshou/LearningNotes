[TOC]

## ASLR

### 一、什么是ASLR

- Address Space Layout Randomization，地址空间布局随机化


- 是一种针对缓冲区溢出的安全保护技术，通过对堆、栈、共享库映射等线性区布局的随机化，通过增加攻击者预测目的地址的难度，防止攻击者直接定位攻击代码位置，达到阻止溢出攻击的目的的一种技术



### 二、iOS4.3开始引入了ASLR技术



### 三、Mach-O的文件结构

![Alt text](./assets/imgs/09-ASLR/1.png "Optional title")



### 四、未使用ASLR

- 函数代码存放在__TEXT段中

- 全局变量存放在__DATA段中

- 可执行文件的内存地址是0x0

- 代码段（__TEXT）的内存地址

  - 就是LC_SEGMENT(__TEXT)中的VM Address
  - arm64：0x100000000（8个0）
  - 非arm64：0x4000（3个0）

- 可以使用 **size -l -m -x** 来查看Mach-O的内存分布

  ```shell
  $ size -l -m -x WeChat.decrypted
  ```

  ![Alt text](./assets/imgs/09-ASLR/2.png "Optional title")


### 五、使用ASLR

- LC_SEGMENT(__TEXT)的VM Address
  - 0x100000000



- ASLR随机产生的Offset（偏移）

  - 0x5000
  - 也就是可执行文件的内存地址

   ![Alt text](./assets/imgs/09-ASLR/3.png "Optional title")



### 六、函数的内存地址

- 函数的内存地址（VM Address） = File Offset + ASLR Offset + __PAGEZERO Size

- 使用 Hopper Disassembler V4 和 ida 64 工具分析的 MachO 文件 中的地址都是是未使用 ASLR 的VM Address，程序装载进入手机，才会产生ASLR。

  - 获得函数的真实内存地址

  ```shell
  # 获取偏移地址 (ASLR Offset地址)
  $ (lldb) image list -o -f
  
  # 快速过滤：获取指定加载模块的偏移地址 (ASLR Offset地址)
  $ (lldb) image list -o -f | grep WeChat
  ```

  ![Alt text](./assets/imgs/09-ASLR/4.png "Optional title")

  ```shell
  # 在某个函数添加断点，函数地址是通过 Hopper 或 IDA 工具分析获取得到
  $ (lldb) breakpoint set -a 函数地址+ASLR Offset地址
  ```

  ![Alt text](./assets/imgs/09-ASLR/5.png "Optional title")



- Hopper、IDA中的地址都是未使用ASLR的VM Address



### 七、函数在MachO文件中位置

- 函数在MachO文件中位置的内存地址（VM Address） = 函数的地址（通过 Hopper 或 IDA 工具分析获取得到 ） -  __PAGEZERO Size（ARM64环境下大小 = 0x1000000、非ARM64环境下大小 = 0x4000）
- 代码段内存区间：**_TEXT**——**_DATA**




### 八、全局变量在MachO文件中的位置

- 全局变量的内存地址（VM Address） = 全局变量的内存地址 - ASLR Offset  -  __PAGEZERO Size

  - 没有使用ASLR的虚拟内存地址 = 全局变量的内存地址 - ASLR Offset

    ![Alt text](./assets/imgs/09-ASLR/6.png "Optional title")



  - 全局变量的内存地址（VM Address） = 没有使用ASLR的虚拟内存地址 - 0x100000000

    ![Alt text](./assets/imgs/09-ASLR/7.png "Optional title")



### 九、寄存器

- **LLDB**指令
  - **memory read** 

    - 读取所有寄存器的值

    ```shell
    # 读取所有寄存器的值
    (lldb) memory read
    
    # 读取 x0 寄存器的值
    (lldb) memory read x0
    ```

  - **memory write**  寄存器  值

    - 给某个寄存器写入值

    ```shell
    (lldb memory) write x0 #0x1
    ```

  - **po $x0**：打印方法调用者

    - $：美元符就可以打印寄存器 x0 的值

    ```shell
    
    # OC调用方法代码
    # BaseMsgContentViewController *vc;
    # [vc touchesBegan_TableView:withEvent:];
    
    # 方法调用代码底层会转成 objc_msgSend(self, _cmd, ···)
    # 第一个参数：vc 方法调用者	= x0
    # 第二个参数：_cmd 方法名	= x1
    # 第三个参数：tableView	= x3
    # 第四个参数：event	= x4
    # ··· 以此类推
    # objc_msgSend(vc, @selector(touchesBegan_TableView:withEvent:), tableView, event)
    
    # x0 = 方法调用者
    # x1 = 方法名(_cmd)
    
    # 读取 x0 寄存器的内容
    (lldb) register read x0
          x0 = 0x00000001372a7e00
     
    # 地址的方式打印方法调用者
    (lldb) po 0x00000001372a7e00
    <baseMsgContentViewController: 0x1372a7e00>
    
    # $寄存器的方式打印方法调用者
    (lldb) po $x0
    <baseMsgContentViewController: 0x1372a7e00>
    ```

  - **x/s $x1**：打印方法名

    - /s：字符串形式读取寄存器 x1 的内存

    ```shell
    x/s $x1
    0x1030ce2e9: "touchesBegan_TableView:withEvent:"
    ```

  - **po $x2**：打印参数（以此类推，**x3、x4**也可能是参数）

  - 如果是**非arm64**，寄存器就是**r0**、**r1**、**r2**