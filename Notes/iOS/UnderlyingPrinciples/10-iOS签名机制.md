[TOC]

## iOS签名机制

### 一、基础知识

#### 1. 学习路线

 ![Snip20180812_1](./assets/imgs/10-iOS签名机制/1.png)



#### 2. 常见英文

- encrypt：加密
- decrypt：解密
- plaintext：明文
- ciphertext：密文



#### 3. 学前须知

- 为了便于学习，设计4个虚拟人物

  - Alice、Bob：互相通信

  - Eve：窃听者

  - Mallory：主动攻击者

    ![2](./assets/imgs/10-iOS签名机制/2.png)

    ​

    ![3](./assets/imgs/10-iOS签名机制/3.png)



#### 4. 如何防止被窃听？

![6](./assets/imgs/10-iOS签名机制/6.png)

#### 5. 如何加密解密？

 ![7](./assets/imgs/10-iOS签名机制/7.png) !7](./assets/imgs/10-iOS签名机制/7.png)



#### 6. 密码的类型

- 根据密钥的使用方法，可以将密码分为2种

  - 对称密码

   ![8](./assets/imgs/10-iOS签名机制/8.png)



  -  公钥密码（非对称）![9](./assets/imgs/10-iOS签名机制/9.png)



### 二、对称密码

#### 1. 对称密码（Symmetric Cryptography）

- 在对称密码中，加密、解密时使用的是同一个密钥

- 常见的对称密码算法有

  - **DES**
  - **3DES**
  - **AES**

   ![10](./assets/imgs/10-iOS签名机制/10.png)



#### 2. DES（Data Encryption Standard）

 ![11](./assets/imgs/10-iOS签名机制/11.png) ![12](./assets/imgs/10-iOS签名机制/12.png)



- **DES**是一种将64bit明文加密成64bit密文的对称密码算法，密钥长度是56bit


- 规格上来说，密钥长度是64bit，但每隔7bit会设置一个用于错误检查的bit，因此密钥长度实质上是56bit


- 由于**DES**每次只能加密64bit的数据，遇到比较大的数据，需要对**DES**加密进行迭代（反复）


- 目前已经可以在短时间内**被破解**，所以**不建议使用**



#### 3. 3DES

- 3DES，将DES重复3次所得到的一种密码算法，也叫做3重DES


- 目前还被一些银行等机构使用，但处理速度不高，安全性逐渐暴露出问题

- 3个密钥都是不同的，也称位**DES-EDE3**

   ![13](./assets/imgs/10-iOS签名机制/13.png) 

![14](./assets/imgs/10-iOS签名机制/14.png)



- 如果所有密钥都使用同一个，则结果与普通的DES是等价的

 ![15](./assets/imgs/10-iOS签名机制/15.png)

- 如果密钥1、密钥3相同，密钥2不同，称为DES-EDE2

 ![16](./assets/imgs/10-iOS签名机制/16.png)



#### 4. AES（Advanced Encryption Standard）

- 取代DES成为新标准的一种对称密码算法


- AES的密钥长度有128、192、256bit三种


- 在2000年时选择Rijindael算法作为AES的实现
- 目前AES，已经逐步取代DES、3DES，成为首选的对称密码算法
- 一般来说，我们也不应该去使用任何自制的密码算法，而是应该使用AES，它经过了全世界密码学家所进行的高品质验证工作





### 三、密钥配送

#### 1. 密钥配送问题

- 在使用对称密码时，一定会遇到密钥配送问题


- 假设，Alice将**使用对称密码加密过的消息**发给了Bob


- 只有将密钥发送给Bob，Bob才能完成解密	


- 在发送密钥过程中，可能会被Eve窃取密钥，最后Eve也能完成解密

 ![17](./assets/imgs/10-iOS签名机制/17.png)



#### 2. 如何解决密钥配送问题

- 有以下几种解决密钥配送的方法
  - 事先共享密钥
  - 密钥分配中心
  - Diffie-Hellman密钥交换
  - **公钥密码**





### 四、公钥密码

#### 1. 公钥密码（Public-key Cryptography）

- 公钥密码中，密钥分为**加密密钥**、**解密密钥**2种，它们并不是同一个密钥


- 公钥密码也被称为**非对称密码（Asymmetric Cryptography**）

- 在公钥密码中

  - 加密密钥，一般是公开的，因此该密钥称为公钥（public key）
  - 解密密钥，由消息接收者自己保管的，不能公开，因此也称为私钥（private key）
  - 公钥和私钥是一一对应的，是不能单独生成的，一对公钥和密钥统称为密钥对（key pair）
  - 由公钥加密的密文，必须使用与该公钥对应的私钥才能解密
  - 由私钥加密的密文，必须使用与该私钥对应的公钥才能解密

   ![18](./assets/imgs/10-iOS签名机制/18.png)

#### 2. 解决密钥配送问题

- 由消息的接收者，生成一对公钥、私钥
- 将公钥发给消息的发送者
- 消息的发送者使用公钥加密消息

 ![19](./assets/imgs/10-iOS签名机制/19.png)



#### 3. RSA

- 目前使用最广泛的公钥密码算法是**RSA**
- **RSA**的名字，由它的3位开发者，即Ron **R**ivest、Adi **S**hamir、Leonard **A**dleman的姓氏首字母组成



### 五、混合加密系统

#### 1.混合密码系统（Hybrid Cryptosystem）

- 对称密码的缺点

  - 不能很好地解决密钥配送问题

- 公钥密码的缺点

  - 加密解密速度比较慢



- 混合密码系统，是将对称密码和公钥密码的优势相结合的方法
  - 解决了公钥密码速度慢的问题

  - 并通过公钥密码解决了对称密码的密钥配送问题



- 网络上的密码通信所用的SSL/TLS都运用了混合密码系统



#### 2. 混合密码-加密

- 会话密钥（session key）
  - 为本次通信随机生成的临时密钥

  - 作为对称密码的密钥，用于加密消息，提高速度

- 加密步骤（发送消息）
  - 1.首先，消息发送者要拥有消息接收者的公钥

  - 2.生成会话密钥，作为对称密码的密钥，加密消息

  - 3.用消息接收者的公钥，加密会话密钥

  - 4.将前2步生成的加密结果，一并发给消息接收者

- 发送出去的内容包括
  - 用会话密钥加密的消息（加密方法：对称密码）
  - 用公钥加密的会话密钥（加密方法：公钥密码）

 ![20](./assets/imgs/10-iOS签名机制/20.png)



#### 3.混合密码-解密

- 解密步骤（收到消息）
  1. 消息接收者用自己的私钥解密出会话密钥
  2. 再用第1步解密出来的会话密钥，解密消息

 ![21](./assets/imgs/10-iOS签名机制/21.png)



#### 4.混合密码-加密解密流程

- Alice>>>>> Bob



  - 发送过程，加密过程

    1.Bob先生成一对公钥、私钥

    2.Bob把公钥共享给Alice

    3.Alice随机生成一个会话密钥（临时密钥）

    4.Alice用会话密钥加密需要发送的消息（使用的是对称密码加密）

    5.Alice用Bob的公钥加密会话密钥（使用的是公钥密码加密，也就是非对称密码加密）

    6.Alice把第4、5步的加密结果，一并发送给Bob

  -                                 接收过程，解密过程 

                                    1.Bob利用自己的私钥解密会话密钥（使用的是公钥密码解密，也就是非对称密码解密）

                                    2.Bob利用会话密钥解密发送过来的消息（使用的是对称密码解密）



### 六、单向散列函数

#### 1.单向散列函数（One-way hash function）

- 单向散列函数，可以根据根据消息内容计算出散列值

- 散列值的长度和消息的长度无关，无论消息是1bit、10M、100G，单向散列函数都会计算出固定长度的散列值

   ![22](./assets/imgs/10-iOS签名机制/22.png)



#### 2.单向散列函数的特点

- 根据任意长度的消息，计算出固定长度的散列值
- 计算速度快，能快速计算出散列值
- 消息不同，散列值也不同
- 具备单向性

 ![23](./assets/imgs/10-iOS签名机制/23.png)

 ![24](./assets/imgs/10-iOS签名机制/24.png)



#### 3. 单向散列函数

- 单向散列函数，又被称为消息摘要函数（message digest function），哈希函数


- 输出的散列值，也被称为消息摘要（message digest）、指纹（fingerprint）


- 常见的几种单向散列函数


  -  MD4、MD5


    -                                                                                                                                                                                                                                                                                 产生128bit的散列值，MD就是Message Digest的缩写，目前已经不安全
    -                                                                                                                                                                                                                                                                                 Mac终端上默认可以使用md5命令

  - SHA-1


    -                                                                                                                                                                                                                                                                                 产生160bit的散列值，目前已经不安全

  - SHA-2


    -  SHA-256、SHA-384、SHA-512，散列值长度分别是256bit、384bit、512bit

  - SHA-3


    - 全新标准



#### 4. 如何防止数据被篡改

 ![25](./assets/imgs/10-iOS签名机制/25.png)

 ![26](./assets/imgs/10-iOS签名机制/26.png)



#### 5. 单向散列函数的应用 – 防止数据被篡改

 ![27](./assets/imgs/10-iOS签名机制/27.png)

- [https://www.realvnc.com/en/connect/download/vnc/](https://www.realvnc.com/en/connect/download/vnc/)

 ![28](./assets/imgs/10-iOS签名机制/28.png)



#### 6. 单向散列函数的应用 – 口令加密

 ![29](./assets/imgs/10-iOS签名机制/29.png)



### 七、数字签名

#### 1. 想象以下场景

 ![30](./assets/imgs/10-iOS签名机制/30.png)

- Alice发的内容有可能是被篡改的，或者有人伪装成Alice发消息，或者就是Alice发的，但她可以否认
- 问题来了：Bob如何确定这段消息的真实性？如何识别篡改、伪装、否认？
- 解决方案

  - 数字签名



#### 2. 数字签名

- 在数字签名技术中，有以下2种行为

  - 生成签名

    -  由消息的发送者完成，通过“签名密钥”生成

  - 验证签名

    -  由消息的接收者完成，通过“验证密钥”验证

- 思考

  - 如何能保证这个签名是消息发送者自己签的？

- 答案

    - 用消息发送者的私钥进行签名



![31](./assets/imgs/10-iOS签名机制/31.png)

在公钥密码中，任何人都可以使用公钥进行加密

![32](./assets/imgs/10-iOS签名机制/32.png)                      

在数字签名中，任何人都可以使用公钥验证签名



#### 3. 数字签名和公钥密码

- 数字签名，其实就是将公钥密码反过来使用

![33](./assets/imgs/10-iOS签名机制/33.png)



#### 4. 数字签名的过程

 ![34](./assets/imgs/10-iOS签名机制/34.png)



#### 5. 数字签名的过程 – 改进

 ![35](./assets/imgs/10-iOS签名机制/35.png)

 ![36](./assets/imgs/10-iOS签名机制/36.png)



#### 6. 数字签名 – 疑惑

- 思考一下
  - 如果有人篡改了文件内容或者签名内容，会是什么结果？
  - 结果是：签名验证失败，证明内容会篡改

- 数字签名不能保证机密性？

  - 数字签名的作用不是为了保证机密性，仅仅是为了能够识别内容有没有被篡改

- 数字签名的作用
  - 确认消息的完整性
  - 识别消息是否被篡改
  - 防止消息发送人否认


#### 7. 数字签名无法解决的问题

- 要正确使用签名，前提是

  - 用于验证签名的公钥必须属于真正的发送者  

- 如果遭遇了中间人攻击，那么

  - 公钥将是伪造的
  - 数字签名将失效

- 所以在验证签名之前，首先得先验证公钥的合法性



- 如何验证公钥的合法性？

  - 证书

   ![37](./assets/imgs/10-iOS签名机制/37.png)







### 八、证书

#### 1. 证书（Certificate）

- 证书，联想的是驾驶证、毕业证、英语四六级证等等，都是由权威机构认证的



- 密码学中的证书，全称叫**公钥证书（Public-keyCertificate，PKC）**，跟驾驶证类似
  - 里面有姓名、邮箱等个人信息，以及此人的公钥
  - 并由认证机构（CertificateAuthority，CA）施加数字签名

- CA就是能够认定“公钥确实属于此人”并能够生成数字签名的个人或者组织
  - 有国际性组织、政府设立的组织
  - 有通过提供认证服务来盈利的企业
  - 个人也可以成立认证机构



#### 2. 证书的利用

 ![38](./assets/imgs/10-iOS签名机制/38.png)

#### 3. 证书的注册和下载

 ![39](./assets/imgs/10-iOS签名机制/39.png)





### 九、iOS签名机制

#### 1.iOS签名机制

- iOS签名机制的作用

  - 保证安装到用户手机上的APP都是经过Apple官方允许的

  ​

- 不管是真机调试，还是发布APP，开发者都需要经过一系列复杂的步骤

  - 生成**CertificateSigningRequest.certSigningRequest**文件
  - 获得**ios_development.cer\ios_distribution.cer**证书文件
  - 注册device、添加AppID
  - 获得***.mobileprovision**文件

  ​

- 对于真机调试，现在的Xcode已经自动帮开发者做了以上操作



- 思考
  - 每一步的作用是什么？
  - **certSigningRequest**、**.cer**、**.mobileprovision**文件究竟里面包含了什么？有何用处？



#### 2. 流程图

 ![40](./assets/imgs/10-iOS签名机制/40.png)



#### 3. 生成Mac设备的公私钥

![41](./assets/imgs/10-iOS签名机制/41.png)



#### 4. 获得证书

 ![42](./assets/imgs/10-iOS签名机制/42.png)

 ![43](./assets/imgs/10-iOS签名机制/43.png)



#### 5. 生成mobileprovision

App ID ： 使用 xxxxx.* 可以匹配任何App

![44](./assets/imgs/10-iOS签名机制/44.png)

- 选中安装的设备![45](./assets/imgs/10-iOS签名机制/45.png)



#### 6. 安全检测

 ![46](./assets/imgs/10-iOS签名机制/46.png)

#### 7. AppStore

- 如果APP是从AppStore下载安装的，你会发现里面是没有mobileprovision文件的

- 它的验证流程会简单很多，大概如下所示

   ![47](./assets/imgs/10-iOS签名机制/47.png)



### 十.重签名

#### 1. 重签名

- 如果希望将破坏了签名的安装包，安装到非越狱的手机上，需要对安装包进行重签名的操作



- 注意
  - 安装包中的可执行文件必须是经过脱壳的，重签名才会有效
  - **.app**包内部的**所有动态库（.framework、.dylib）、AppExtension（PlugIns文件夹，拓展名是appex）、WatchApp（Watch文件夹）**都需要重新签名



- 重签名打包后，安装到设备的过程中，可能需要经常查看设备的日志信息
  - 程序运行过程中：**Window -> Devices and Simulators -> View Device Logs**
  - 程序安装过程中： **Window -> Devices and Simulators -> Open Console**



#### 2. 重签名步骤

- 准备工作

1. 给Mach-O脱壳
2. 从iPhone里拷贝出 **wechat.app**到Mac上
3. 新建 **Payload**文件夹 -> 将**wechat.app**放进去 
4. 准备一个**embedded.mobileprovision**文件（必须是付费证书产生的，appid、device一定要匹配），并放入**.app**包中
5. 可以通过Xcode自动生成，

   - 打开Xcode -> 新建iOS工程 -> 输入Bundle Identifier:   * (使用 * 通配符可以匹配任意App)  -> 选中Signing 证书  -> 编译 -> 找到左侧文件夹导航条中 **Products** 文件夹中的APP -> Show in Finder然后从文件中找到 **embedded.mobileprovision**

   - 可以去开发者证书网站生成下载

     - <https://developer.apple.com/>



- 从**embedded.mobileprovision**文件中提取出**entitlements.plist**权限文件

  ```shell
  # 通过securitycms指令，提取temp.plist文件
  security cms -D -i embedded.mobileprovision > temp.plist
  
  # 通过PlistBuddy指令将temp.plist转成Entitlements格式的plist文件
  /usr/libexec/PlistBuddy -x -c 'Print :Entitlements' temp.plist > entitlements.plist
  ```

  将 **entitlements.plist**放入到 xxx.app 同级的文件路径

![48](./assets/imgs/10-iOS签名机制/48.png)

​	**embedded.mobileprovision**所在文件路径	![49](./assets/imgs/10-iOS签名机制/49.png)


- 查看可用的证书

  ```shell
  $ security find-identity -v -p codesigning
  ```


- 对.app内部的动态库、AppExtension等进行签名

  ```shell
  $ codesign -fs 证书ID xxx.dylib
  ```

- 对.app包进行签名

  ```shell
  # 方式一
  $ codesign -f -s  证书ID --entitlements entitlements.plist xxx.app
  # 方式二
  $ codesign -fs  证书ID --entitlements entitlements.plist xxx.app
  ```

  ```shell
  # 对.app文件签名
  # 写法一
  $ codesign -f -s 证书ID --entitlements entitlements.plist tweaktest.app 
  
  # 写法二
  $ codesign -fs 证书ID --entitlements entitlements.plist tweaktest.app
  ```


- 签名替换成功，移除  entitlements.plist ，然后再压缩Playload 文件 -> Playload.zip ->  改后缀名.ipa格式(Playload.ipa) 安装到未越狱手机


  ![50](./assets/imgs/10-iOS签名机制/50.png)


- entitlements.plist 不在xxx.app的同级目录中，需要在 entitlements.plist 所在路径执行下面指令，指定 /Playload/xxx.app 路径

  ```shell
  $ codesign -fs  证书ID --entitlements entitlements.plist xxx.app /Playload/xxx.app
  ```




### 十一、重签名GUI工具

- iOS App Signer(**推荐使用，一步操作**)

  - [https://github.com/DanTheMan827/ios-app-signer](https://github.com/DanTheMan827/ios-app-signer)

  - 可以对.app重签名打包成ipa

  - 只需要在.app包中提供对应的**embedded.mobileprovision**文件 

    - **embedded.mobileprovision**放到.app文件中

  - 不需要**entitlements.plist**文件

    - 选中.app就可以重签名，就可以得到.ipa

    - 从github下载，用Xcode打开，把debug修改release，再运行，得到一个iOS App Signer.app

    ​



- iReSign
  - [https://github.com/maciekish/iReSign](https://github.com/maciekish/iReSign)
  - 可以对ipa进行重签名
  - 需要提供**entitlements.plist**、**embedded.mobileprovision**文件的路径
  - 可以修改id




- 工具注意事项

  - 使用这个两个签名工具，只会对Payload最外层的.app包进行重签名

  - 不会对动态库重签名

    ​

    ​



### 十二、动态库注入

- iPhone中tweak_test.dylib动态库路径

  - /Device/Library/MobileSubstrate/DynamicLibraries/tweak_test.dylib
  - 将tweak_test.dylib放到xxx.app文件中

   ![51](./assets/imgs/10-iOS签名机制/51.png)



-     可以使用insert_dylib库将动态库注入到Mach-O文件中

      - [https://github.com/Tyilo/insert_dylib](https://github.com/Tyilo/insert_dylib)
      - 下载好了，选择Xcode打开 -> 将Debug修改Release -> 运行 -> 得到 **insert_dylib** 可执行文件 -> 放入 **/usr/local/bin** 文件中



- 用法

     - `insert_dylib 动态库加载路径（要求完整路径） Mach-O文件`

         - 例子：

         ```shell
         # @executable_path 使用环境变量表示完整路径使用
          $ insert_dylib @executable_path/需要注入动态库文件.dylib  Mach-O文件
         
         # tweaktest_patched already exists. Overwrite it? [y/n] $ y
         
         # LC_CODE_SIGNATURE load command found. Remove it? [y/n] $ y
         ```

     - 有2个常用参数选项
       - --weak，即使动态库找不到也不会报错，App也不会闪退

         ```shell
         # --weak参数使用
         $ insert_dylib @executable_path/需要注入动态库文件.dylib Mach-O文件 --weak
         ```

       - --all-yes，后面所有的选择都为yes

         ``` shell
         $ insert_dylib @executable_path/需要注入动态库文件.dylib Mach-O文件 --all-yes
         ```

       - 直接Mach-O文件覆盖操作

         ```shell
         $ insert_dylib @executable_path/需要注入动态库文件.dylib Mach-O文件 --all-yes --weak Mach-O文件
         ```



- **insert_dylib**的本质是往**Mach-O**文件的**Load Commands**中添加了一个**LC_LOAD_DYLIB**或**LC_LOAD_WEAK_DYLIB**



- 可以通过otool查看Mach-O的动态库依赖信息

  ```shell
  # 需要注入动态库文件.dylib 也是Mach-O文件
  $ otool -L 需要注入动态库文件.dylib
  ```



### 十三、更改动态库加载地址


- 通过Theos开发的动态库插件（dylib）
  - 默认都依赖于 **/Library/Frameworks/CydiaSubstrate.framework/CydiaSubstrate**
  - 如果要将动态库插件打包到**ipa**中，也需要将**CydiaSubstrate**打包到**ipa**中，并且修改下**CydiaSubstrate**的加载地址


 ![52](./assets/imgs/10-iOS签名机制/52.png)



- 可以使用**install_name_tool**修改**Mach-O**文件中动态库的加载地址（Mac自带的工具）

  **install_name_tool -change  旧地址 新地址  加载动态库文件**

  - 例子

    ```shell
    $ install_name_tool -change /Library/Frameworks/CydiaSubstrate/framework/CydidaSubstrate @loader_path/CydiaSubstrate 需要注入动态库文件.dylib
    ```


- 2个常用环境变量
  - **@executable_path**代表可执行文件所在的目录
  - **@loader_path**代表动态库所在的目录





### 十四、总结

 ![53](./assets/imgs/10-iOS签名机制/53.png)

