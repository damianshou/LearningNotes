[TOC]

## isa & superclass

### isa指针

![01](./assets/imgs/14-isa&superclass/01.png)

- instance的isa指向class

  - 当调用对象方法时，通过instance的isa找到class，最后找到对象方法的实现进行调用

- class的isa指向meta-class

  - 当调用类方法时，通过class的isa找到meta-class，最后找到类方法的实现进行调用



### class对象的superclass指针

![02](./assets/imgs/14-isa&superclass/02.png)

- 当Student的instance对象要调用Person的对象方法时，会先通过isa找到Student的class，然后通过superclass找到Person的class，最后找到对象方法的实现进行调用



### meta-class对象的superclass指针

![03](./assets/imgs/14-isa&superclass/03.png)

- 当Student的class要调用Person的类方法时，会先通过isa找到Student的meta-class，然后通过superclass找到Person的meta-class，最后找到类方法的实现进行调用



### isa、superclass总结

![04](./assets/imgs/14-isa&superclass/04.png)

- instance的isa指向class
- class的isa指向meta-class
- meta-class的isa指向基类的meta-class
- class的superclass指向父类的class
  - 如果没有父类，superclass指针为nil
- meta-class的superclass指向父类的meta-class
  - 基类的meta-class的superclass指向基类的class
- instance调用对象方法的轨迹
  - isa找到class，方法不存在，就通过superclass找父类
- class调用类方法的轨迹
  - isa找meta-class，方法不存在，就通过superclass找父类



### &ISA_MASK

![05](./assets/imgs/14-isa&superclass/05.png)

- 从64bit开始，isa需要进行一次位运算，才能计算出真实地址
- ISA_MASK：在objc4-750.tar.gz源码里搜索获取



### objc4源码下载

https://opensource.apple.com/tarballs/objc4/

![06](./assets/imgs/14-isa&superclass/06.png)

- class、meta-class对象的本质结构都是struct objc_class



### 窥探struct objc_class的结构

![07](./assets/imgs/14-isa&superclass/07.png)



