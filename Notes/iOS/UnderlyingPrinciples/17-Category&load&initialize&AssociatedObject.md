[TOC]

## Category & load & initialize & AssociatedObject

### Category

#### 底层结构

> 定义在objc-runtime-new.h中

![01](./assets/imgs/17-Category&load&initialize&AssociatedObject/01.png)



#### 加载处理过程

1. 通过Runtime加载某个类的所有Category数据
2. 把所有Category的方法、属性、协议数据，合并到一个大数组中
   - 后面参与编译的Category数据，会在数组的前面
3. 将合并后的分类数据（方法、属性、协议），插入到类原来数据的前面



- 源码解读顺序
  - objc-os.mm
  - _objc_init
  - map_images
  - map_images_nolock

- objc-runtime-new.mm
  - _read_images
  - remethodizeClass
  - attachCategories
  - attachLists
  - realloc、memmove、 memcpy



### +load方法

- +load方法会在runtime加载类、分类时调用
- 每个类、分类的+load，在程序运行过程中只调用一次
- 调用顺序
  1. 先调用类的+load
     - 按照编译先后顺序调用（先编译，先调用）
     - 调用子类的+load之前会先调用父类的+load

  2. 再调用分类的+load
     - 按照编译先后顺序调用（先编译，先调用）



- objc4源码解读过程：objc-os.mm
  - _objc_init
  - load_images
  - prepare_load_methods
  - schedule_class_load
  - add_class_to_loadable_list
  - add_category_to_loadable_list
  - call_load_methods
  - call_class_loads
  - call_category_loads
  - (*load_method)(cls, SEL_load)

- +load方法是根据方法地址直接调用，并不是经过objc_msgSend函数调用



### +initialize方法

- +initialize方法会在类第一次接收到消息时调用

- 调用顺序

  - 先调用父类的+initialize，再调用子类的+initialize
  - 先初始化父类，再初始化子类，每个类只会初始化1次




- objc4源码解读过程

  - objc-msg-arm64.s

    - objc_msgSend

- objc-runtime-new.mm

  - class_getInstanceMethod
  - lookUpImpOrNil
  - lookUpImpOrForward
  - _class_initialize
  - callInitialize
  - objc_msgSend(cls, SEL_initialize)



- +initialize和+load的很大区别是，+initialize是通过objc_msgSend进行调用的，所以有以下特点
  - 如果子类没有实现+initialize，会调用父类的+initialize（所以父类的+initialize可能会被调用多次）
  - 如果分类实现了+initialize，就覆盖类本身的+initialize调用



### 思考：如何实现给分类“添加成员变量”？

```objective-c
// main.m
#import <Foundation/Foundation.h>
#import "Person.h"
#import "Person+Test.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        Person *person = [[Person alloc] init];
        person.age = 10;
        person.weight = 40;
        
        Person *person2 = [[Person alloc] init];
        person2.age = 20;    // 20是存储在peron2对象内部
        person2.weight = 50; // 50是存放在全局的字典对象里面
        
        NSLog(@"person - age is %d, weight is %d", person.age, person.weight);
        NSLog(@"person2 - age is %d, weight is %d", person2.age, person2.weight);
    }
    return 0;
}
```

```objective-c
//  Person

#import <Foundation/Foundation.h>

@interface Person : NSObject

@property (assign, nonatomic) int age;

@end

@implementation Person

@end
```



#### 方式一、使用全局变量

```objective-c
// Person+Test

#import "Person.h"

@interface Person (Test)

@property (assign, nonatomic) int weight; // 只会生成getter & setter，不会生成_weight

@end
    
@implementation Person (Test)
int weight_; 
- (void)setWeight:(int)weight {
    weight_ = weight;
}

- (int)weight {
    return weight_;
}
 
@end
```

- 存在问题：

  - 内存泄漏，weight_ 一直都在
  - weight_  只有一份，多个对象访问一个值



#### 方式二、使用字典

```objective-c
// Person+Test

#import "Person.h"

@interface Person (Test)

@property (assign, nonatomic) int weight; // 只会生成getter & setter，不会生成_weight
@property (copy, nonatomic) NSString* name;

@end
    
#define KEY [NSString stringWithFormat:@"%p", self]

@implementation Person (Test)
    
NSMutableDictionary *names_;   
NSMutableDictionary *weights_;

+ (void)load {
    weights_ = [NSMutableDictionary dictionary];
    names_ = [NSMutableDictionary dictionary];
}

- (void)setWeight:(int)weight {
   weights_[Key] = @(weight);
}

- (int)weight {
    return [weights_[Key] intValue];
}

- (void)setName:(NSString *)name {
    names_[KEY] = name;
}

- (NSString *)name {
    return names_[KEY];
}
 
@end
```

- 存在问题
  - 线程安全：同一个对象，在不同线程中同时访问



### AssociatedObject - 关联对象

> 默认情况下，因为分类底层结构的限制，不能添加成员变量到分类中，但可以通过关联对象来间接实现。



#### 关联对象提供了以下API

- 添加关联对象

  ```objective-c
  void objc_setAssociatedObject(id object, 
                                const void * key,
                                id value, 
                                objc_AssociationPolicy policy)
  ```

- 获得关联对象

  ```objective-c
  id objc_getAssociatedObject(id object, const void * key)
  ```

- 移除所有的关联对象

  ```objective-c
  void objc_removeAssociatedObjects(id object)
  ```



#### key的常见用法

```objective-c
static void *MyKey = &MyKey;
objc_setAssociatedObject(obj, MyKey, value, OBJC_ASSOCIATION_RETAIN_NONATOMIC)
objc_getAssociatedObject(obj, MyKey)

static char MyKey;
objc_setAssociatedObject(obj, &MyKey, value, OBJC_ASSOCIATION_RETAIN_NONATOMIC)
objc_getAssociatedObject(obj, &MyKey)

// 使用属性名作为key
objc_setAssociatedObject(obj, @"property", value, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
objc_getAssociatedObject(obj, @"property");

// 使用get方法的@selecor作为key
objc_setAssociatedObject(obj,
                         @selector(getter),
                         value,
                         OBJC_ASSOCIATION_RETAIN_NONATOMIC)
objc_getAssociatedObject(obj, @selector(getter))

```



#### 方式三、使用关联对象

```objective-c
// Person+Test

#import "Person.h"
#import <objc/runtime.h>

@interface Person (Test)

@property (assign, nonatomic) int weight; // 只会生成getter & setter，不会生成_weight
@property (copy, nonatomic) NSString* name;

@end
    
#define KEY [NSString stringWithFormat:@"%p", self]

@implementation Person (Test)
    
/* 
// 方案一：单字符，节省内存空间
static const char WeightKey;
static const char NameKey;

- (void)setWeight:(int)weight {
   objc_setAssociatedObject(self,
   							&WeightKey,
                            @(weight), 
                            OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (int)weight {
   return [objc_getAssociatedObject(self, &WeightKey) intValue];
}

- (void)setName:(NSString *)name {
   objc_setAssociatedObject(self, &NameKey, name, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (NSString *)name {
   return objc_getAssociatedObject(self, &NameKey);
}
*/
    
/* 
// 方案二：指针
static const void *WeightKey = &WeightKey;
static const void *NameKey = &NameKey;

- (void)setWeight:(int)weight {
   objc_setAssociatedObject(self, 
   							WeightKey, 
   							@(weight), 
   							OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (int)weight {
   return [objc_getAssociatedObject(self, WeightKey) intValue];
}

- (void)setName:(NSString *)name {
   objc_setAssociatedObject(self, NameKey, name, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (NSString *)name {
   return objc_getAssociatedObject(self, NameKey);
}
*/
    
/* 
// 方案三：字符串
// @"name" 是字符串常量，在常量区，内存只有一份，@"name" == @"name"

#define WeightKey @"weight"
#define NameKey @"name" 

- (void)setWeight:(int)weight {
   objc_setAssociatedObject(self, 
   							WeightKey, 
   							@(weight), 
   							OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (int)weight {
   return [objc_getAssociatedObject(self, WeightKey) intValue];
}

- (void)setName:(NSString *)name {
   objc_setAssociatedObject(self, NameKey, name, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (NSString *)name {
   return objc_getAssociatedObject(self, NameKey);
}
*/
    
// 方案四：可读性高，代码精简，不用维护其他的宏、变量等(推荐)
- (void)setWeight:(int)weight {
    objc_setAssociatedObject(self, 
                             @selector(weight), 
                             @(weight), 
                             OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (int)weight {
    // 隐式参数
    // _cmd == @selector(weight)
    return [objc_getAssociatedObject(self, _cmd) intValue];
}

- (void)setName:(NSString *)name {
    objc_setAssociatedObject(self, 
                             @selector(name), 
                             name, 
                             OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (NSString *)name {
    // 隐式参数
    // _cmd == @selector(name)
    return objc_getAssociatedObject(self, _cmd);
}
 
@end
```



#### objc_AssociationPolicy 相关属性

| objc_AssociationPolicy            | 对应的修饰符      |
| --------------------------------- | ----------------- |
| OBJC_ASSOCIATION_ASSIGN           | assign            |
| OBJC_ASSOCIATION_RETAIN_NONATOMIC | strong, nonatomic |
| OBJC_ASSOCIATION_COPY_NONATOMIC   | copy, nonatomic   |
| OBJC_ASSOCIATION_RETAIN           | strong, atomic    |
| OBJC_ASSOCIATION_COPY             | copy, atomic      |



#### 关联对象原理 

实现关联对象技术的核心对象有

- AssociationsManager
- AssociationsHashMap
- ObjectAssociationMap
- ObjcAssociation



objc4源码解读：objc-references.mm

![02](./assets/imgs/17-Category&load&initialize&AssociatedObject/02.png)

![03](./assets/imgs/17-Category&load&initialize&AssociatedObject/03.png)



### 面试题

#### 1. Category的使用场合是什么？

扩展原类的功能，自定义方法



#### 2. Category的实现原理

Category编译之后的底层结构是struct category_t，里面存储着分类的对象方法、类方法、属性、协议信息
在程序运行的时候，runtime会将Category的数据，合并到类信息中（类对象、元类对象中）



#### 3. Category和Class Extension的区别是什么？

Class Extension在编译的时候，它的数据就已经包含在类信息中
Category是在运行时，才会将数据合并到类信息中



#### 4. Category中有load方法吗？load方法是什么时候调用的？load 方法能继承吗？

1. 有load方法
2. load方法在runtime加载类、分类的时候调用
3. load方法可以继承，但是一般情况下不会主动去调用load方法，都是让系统自动调用



#### 5. load、initialize方法的区别什么？它们在category中的调用的顺序？以及出现继承时他们之间的调用过程？

load、initialize方法的区别什么？

- 调用方式

  - load是根据函数地址直接调用
  - initialize是通过objc_msgSend调用

- 调用时刻

  - load是runtime加载类、分类的时候调用（只会调用1次）
  - initialize是类第一次接收到消息的时候调用，每一个类只会initialize一次（父类的initialize方法可能会被调用多次）


load、initialize的调用顺序？

- load
  - 先调用类的load
    - 先编译的类，优先调用load
    - 调用子类的load之前，会先调用父类的load
  - 再调用分类的load
    - 先编译的分类，优先调用load
- initialize
  - 先初始化父类
  - 再初始化子类（可能最终调用的是父类的initialize方法）



#### 6. Category能否添加成员变量？如果可以，如何给Category添加成员变量？

- 不能直接给Category添加成员变量，但是可以间接实现Category有成员变量的效果（使用关联对象）